package amazon;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

// Amazon question from coding challenge
// Given N locations of steak houses, return the M closest steak houses
// The performance is N log M
public class SteakHouses {

    public static void main(String[] args) {
        // [(1,-3),(1,2),(3,4)], 1 (find closest)
        List<List<Integer>> x = new ArrayList<>();
        List<Integer> c;
        c = new ArrayList<>();
        c.add(1);
        c.add(-3);
        x.add(c);
        c = new ArrayList<>();
        c.add(1);
        c.add(2);
        x.add(c);
        c = new ArrayList<>();
        c.add(3);
        c.add(4);
        x.add(c);

        List<List<Integer>> ret = nearestXsteakHouses(3, x, 1);
        for(List<Integer> pair : ret)
            System.out.println(pair.get(0) + ", " + pair.get(1));
        System.out.println();

        // [(3,6),(2,4),(5,3),(2,7),(1,8),(7,9)], 3 (find the 3 closest)
        x = new ArrayList<>();
        c = new ArrayList<>();
        c.add(3);
        c.add(6);
        x.add(c);
        c = new ArrayList<>();
        c.add(2);
        c.add(4);
        x.add(c);
        c = new ArrayList<>();
        c.add(5);
        c.add(3);
        x.add(c);
        c = new ArrayList<>();
        c.add(2);
        c.add(7);
        x.add(c);
        c = new ArrayList<>();
        c.add(1);
        c.add(8);
        x.add(c);
        c = new ArrayList<>();
        c.add(7);
        c.add(9);
        x.add(c);

        ret = nearestXsteakHouses(6, x, 3);

        for(List<Integer> pair : ret)
            System.out.println(pair.get(0) + ", " + pair.get(1));
    }

    static List<List<Integer>> nearestXsteakHouses(int totalSteakhouses,
                                                   List<List<Integer>> allLocations,
                                                   int numSteakhouses) {
        ArrayList<Node> heap = new ArrayList<>(numSteakhouses);

        for(int i = 0; i < totalSteakhouses; i++) {
            double distance = getDistance(allLocations.get(i));
            if (i < numSteakhouses) {
                insert(new Node(distance, allLocations.get(i)), heap);
                //System.out.println(toString(heap));
            } else {
                if(distance < heap.get(0).distance) {
                    //heap.set(0, new Node(distance, allLocations.get(i)));
                    //maxHeapify(0, heap);
                    heap.remove(0);
                    insert(new Node(distance, allLocations.get(i)), heap);
                }
            }
        }

        System.out.println(toString(heap));
        //return getReturnLis2(heap);  // with JDK sort
        return getReturnList(heap);
    }

    static void insert(Node node, ArrayList<Node> heap) {
        heap.add(node);
        int current = heap.size() - 1;
        int parent = getParentIndex(current);

        while (heap.get(current).distance > heap.get(parent).distance) {
            swap(current, parent, heap);
            current = parent;
            parent = getParentIndex(current);
        }
    }

    static void maxHeapify(int i, ArrayList<Node> heap){
        int left = getLeftChild(i);  // 1 (89) / 3 (36)
        int right = getRightChild(i); // 2 (72) / 4 (75)
        int largest = i; // 0 (63) / 1 (63)

        if(left < heap.size() && // makes sure a left leaf exists
                heap.get(left).distance > heap.get(largest).distance)
            largest = left;
        if(right < heap.size() &&
                heap.get(right).distance > heap.get(largest).distance)
            largest = right;

        if(largest != i) {  // if current node is larger than child nodes, exit recursion
            swap(i, largest, heap); // 0 (63), 1 (89) / 1 (63), 4 (75)
            maxHeapify(largest, heap);  // 1, 4 ... moves down the sub heap
        }
    }

    static double getDistance(List<Integer> location) {
        return Math.sqrt(Math.pow(location.get(0), 2) + Math.pow(location.get(1), 2));
    }

    static int getParentIndex(int index) {
        return (index-1) / 2;
    }

    static int getLeftChild(int index) {
        return index * 2 + 1;
    }

    static int getRightChild(int index) {
        return index * 2 + 2;
    }

    static void swap(int i, int j, ArrayList<Node> heap) {
        Node temp = heap.get(i);
        heap.set(i, heap.get(j));
        heap.set(j, temp);
    }

    static List<List<Integer>> getReturnList(ArrayList<Node> nodes) {

        LinkedList<List<Integer>> ret = new LinkedList<>();
        while(nodes.size() > 0) {
            Node node = pop(nodes);
            ret.addFirst(node.coordinate);
        }

        return ret;
    }

    static List<List<Integer>> getReturnList2(ArrayList<Node> nodes) {
        Collections.sort(nodes);

        List<List<Integer>> ret = new ArrayList<>(nodes.size());
        for (Node n: nodes) {
            ret.add(n.coordinate);
        }

        return ret;
    }

    static Node pop(ArrayList<Node> heap) {
        Node root = heap.get(0);

        swap(0, heap.size() - 1, heap);
        heap.remove(heap.size() - 1);
        maxHeapify(0, heap);

        return root;
    }

    static String toString(ArrayList<Node> heap) {
        StringBuilder s = new StringBuilder(heap.size() * 20);

        for (Node n : heap) {
            s.append(n != null ? n.toString() : "null").append("\n");
        }

        return s.toString();
    }

    private static class Node implements Comparable<Node> {
        List<Integer> coordinate;
        double distance;

        public Node(double distance, List<Integer> coordinate) {
            this.distance = distance;
            this.coordinate = coordinate;
        }

        public String toString() {
            return distance + ", [" + coordinate.get(0) + ", " + coordinate.get(1) + "]";
        }

        @Override
        public int compareTo(Node o) {
            if(distance == o.distance)
                return 0;
            else
                return (distance > o.distance) ? 1 : -1;
        }
    }
}
