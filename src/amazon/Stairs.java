package amazon;

import java.util.LinkedList;

/*
    *****************

    Source: https://www.youtube.com/watch?v=5o-kdjv7FD0
    Find the permutation of steps required to reach N steps using either 1 or 2 steps at a time
    Secondly, try solving it with a configurable set of steps allowed, e.g. {1, 3, 5}

    My solution can be optimized for the 1,2 steps, since it follows the Fibonacci sequence:
    https://www.youtube.com/watch?v=HmiEHhSCx0U
 */

public class Stairs {

    public static void main(String[] args) {
        //System.out.println(num_ways(3) + num_ways(1));
        /*System.out.println(num_ways(2));
        System.out.println(num_ways(3));
        System.out.println(num_ways(4));
        System.out.println(num_ways(5));
        System.out.println(num_ways(6));*/

        System.out.println(num_ways2(6));
        System.out.println(num_ways2(6, new int[]{1, 2}));

        //System.out.println(num_ways(5));  // num_ways(n) = num_ways(n-1) + num_ways(n-2)
        System.out.println(num_ways(5, new int[]{1,3,5}));

        System.out.println();
        System.out.println(num_ways2(5, new int[]{1,3,5}));
    }

    static int sum(LinkedList<Integer> steps) {
        int sum = 0;
        for(int x : steps)
            sum += x;

        return sum;
    }

    // bottom up (because we start from 0 to N)
    static int num_ways(int N) {
        System.out.println();
        LinkedList<LinkedList<Integer>> save = num_ways(N, new LinkedList<>(), new LinkedList<>());

        return save.size();
    }

    static LinkedList<LinkedList<Integer>> num_ways(int N, LinkedList<LinkedList<Integer>> save, LinkedList<Integer> steps) {
        int sum = sum(steps);

        if(sum < N - 1) {
            LinkedList<Integer> newSteps = new LinkedList<>();
            newSteps.addAll(steps);
            newSteps.add(2);
            num_ways(N, save, newSteps);
        }

        if(sum < N) {
            LinkedList<Integer> newSteps = new LinkedList<>();
            newSteps.addAll(steps);
            newSteps.add(1);
            num_ways(N, save, newSteps);
        }

        if(sum == N) {
            save.add(steps);
            for (int x : steps)
                System.out.print(x);
            System.out.println();
        }

        return save;
    }

    // bottom up
    static int num_ways(int N, int[] stepsAllowed) {
        LinkedList<LinkedList<Integer>> save = num_ways(N, stepsAllowed, new LinkedList<>(), new LinkedList<>());

        return save.size();
    }

    static LinkedList<LinkedList<Integer>> num_ways(int N, int[] stepsAllowed, LinkedList<LinkedList<Integer>> save, LinkedList<Integer> steps) {
        int sum = sum(steps);

        for(int x : stepsAllowed)
            if(sum < N - (x-1)) {
                //System.out.println(N + " - " + x + " = " + (N-x));
                LinkedList<Integer> newSteps = new LinkedList<>();
                newSteps.addAll(steps);
                newSteps.add(x);
                num_ways(N, stepsAllowed, save, newSteps);
            }

        if(sum == N) {
            save.add(steps);
            for (int x : steps)
                System.out.print(x);
            System.out.println();
        }

        return save;
    }

    // top down (because execution stars at N and works its way down to the base cases)
    static int num_ways2(int N) {
        if(N == 0)
            return 0;
        else if(N == 1)
            return 1;
        else if(N == 2)
            return 2;
        return num_ways2(N - 1) + num_ways2(N - 2);
    }

    // top down
    static int num_ways2(int N, int[] stepsAllowed) {
        if(N == 0)
            return 0;

        for(int i = 0; i < stepsAllowed.length; i++)
            if(N == stepsAllowed[i])
                return stepsAllowed[i];

        return num_ways2(N - 1) + num_ways2(N - 2);
    }
}
