import java.util.LinkedList;

// K Closest Points to the Origin (0, 0)
// source: https://www.youtube.com/watch?v=eaYX0Ee0Kcg&t=238s
/*
Two optimizations: (1) We don't need the actual distances, just relative distances. So, skip calculating the square
roots (computationally heavy). just store the sum of squares as "distance". (2) No need to create the entire
points_with_d array and storing it. Create the max heap right away and put/replace elements into the heap as you
calculate the "distances". The heap still has points and distances, but only k of them
 */
public class KClosestPoints {

    private static class Point {
        int x, y;
        double distance;

        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public String toString() {
            return "("+x+","+y+": ["+ distance +"])";
        }
    }

    private static class MinBinaryHeap {
        Point[] heap;
        int count = 0;
        public MinBinaryHeap(int size)
        {
            heap = new Point[size];
        }

        public int getParentIndex(int index) {
            if(index % 2 == 0)
                return (index - 1) / 2;
            return index / 2;
        }

        public int getLeftChildIndex(int index) {
            return index * 2 + 1;
        }

        public int getRightChildIndex(int index) {
            return index * 2 + 2;
        }

        public void insert(Point point) {
            heap[count] = point;
            // bubble-up
            int i = count;
            count += 1;

            while(i > 0) {
                int p = getParentIndex(i);
                if(heap[i].distance < heap[p].distance)
                    swap(heap, i, p);
                i = p;
            }
        }

        public Point popMin() throws Exception {
            if(count == 0)
                throw new Exception("heap is empty!");
            count -= 1;
            int last = count;
            Point ret = heap[0];
            heap[0] = heap[last];  // swap the first with the last
            heap[last] = null;

            int i = 0;
            int leftChildIndex = getLeftChildIndex(i);
            int rightChildIndex = getRightChildIndex(i);

            while(leftChildIndex < count) {
                Point smallest = heap[i];
                Point leftChild = heap[leftChildIndex];
                Point rightChild;
                if(rightChildIndex >= count)
                    rightChild = null;
                else
                    rightChild = heap[rightChildIndex];

                if(smallest.distance < leftChild.distance && (rightChild == null || smallest.distance < rightChild.distance))  // subtree satisfies minheap
                    break;  // done

                if(rightChild == null || leftChild.distance < rightChild.distance) {
                    swap(heap, i, leftChildIndex);
                    i = leftChildIndex;
                } else {
                    swap(heap, i, rightChildIndex);
                    i = rightChildIndex;
                }

                leftChildIndex = getLeftChildIndex(i);
                rightChildIndex = getRightChildIndex(i);
            }

            return ret;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();

            for(int i = 0; i < count; i++)
                sb.append(heap[i]).append(", ");

            if(sb.length() > 0)
                return sb.substring(0, sb.lastIndexOf(", "));
            else
                return "";
        }
    }

    // Pythagorean theorem in 2D space
    // square root is computationally heavy
    static double getDistance(Point p1, Point p2) {
        return Math.sqrt(Math.pow(p1.x - p2.x,2) + Math.pow(p1.y - p2.y, 2));
    }

    // gives the relative distance; much more efficient than computing the Pythagorean theorem
    static double getSumOfSquares(Point p1, Point p2) {
        return Math.pow(p1.x - p2.x,2) + Math.pow(p1.y - p2.y, 2);
    }

    static void selectionSort(Point[] points) {
        for(int i = 0; i < points.length - 1; i++) {
            for(int j = i + 1; j < points.length; j++ ) {
                if(points[i].distance > points[j].distance)
                    swap(points, i, j);
            }
        }
    }

    static void swap(Point[] points, int i, int j) {
        Point temp = points[i];
        points[i] = points[j];
        points[j] = temp;
    }

    public static void main(String[] args) throws Exception {
        Point[] points = new Point[6];
        points[0] = new Point(-2, 4);
        points[1] = new Point(0, -2);
        points[2] = new Point(-1, 0);
        points[3] = new Point(3, 5);
        points[4] = new Point(-2, -3);
        points[5] = new Point(3, 2);

        for(Point p : points)
            System.out.println(p);


        int k = 3;
        LinkedList<Point> kShortestDistances = bruteForceSolution(points, k);
        for(Point p : kShortestDistances)
            System.out.println(p);

        kShortestDistances = bruteForceSolutionWithRelativeDistance(points, k);
        for(Point p  : kShortestDistances)
            System.out.println(p);

        kShortestDistances = bestSolution(points, k);
        for(Point p  : kShortestDistances)
            System.out.println(p);
    }

    static LinkedList<Point> bruteForceSolution(Point[] points, int k) {
        Point origin = new Point(0, 0);
        for(int i = 0; i < points.length; i++) {
            points[i].distance = getDistance(origin, points[i]);
        }

        selectionSort(points);

        LinkedList<Point> kShortestDistances = new LinkedList<>();
        int c = 0;
        while(c < k && c < points.length) {
            kShortestDistances.add(points[c]);
            c++;
        }

        return kShortestDistances;
    }

    static LinkedList<Point> bruteForceSolutionWithRelativeDistance(Point[] points, int k) {
        Point origin = new Point(0, 0);
        for(int i = 0; i < points.length; i++) {
            points[i].distance = getSumOfSquares(origin, points[i]);
        }

        selectionSort(points);

        LinkedList<Point> kShortestDistances = new LinkedList<>();
        int c = 0;
        while(c < k && c < points.length) {
            kShortestDistances.add(points[c]);
            c++;
        }

        return kShortestDistances;
    }

    static LinkedList<Point> bestSolution(Point[] points, int k) throws Exception {
        Point origin = new Point(0, 0);
        for(int i = 0; i < points.length; i++) {
            points[i].distance = getSumOfSquares(origin, points[i]);
        }

        System.out.println();


        MinBinaryHeap minBinaryHeap = new MinBinaryHeap(points.length);
        for(Point p : points) {
            System.out.println(p);
            minBinaryHeap.insert(p);
        }

        System.out.println();
        System.out.println(minBinaryHeap);
        System.out.println();

        LinkedList<Point> kShortestDistances = new LinkedList<>();
        for(int i = 0; i < k; i++)
            kShortestDistances.add(minBinaryHeap.popMin());

        return kShortestDistances;
    }
}
