
// Source: https://youtu.be/B6Tsrmgsy3k

import java.util.LinkedList;

public class FindIntersectionBetweenArrays {
    public static void main(String[] args) {
        int[] arr1 = new int[]{2, 6, 9, 11, 13, 17};
        int[] arr2 = new int[]{3, 6, 7, 10, 13, 18};
        int[] arr3 = new int[]{4, 5, 6, 9, 11, 13};


        LinkedList<Integer> intersection = solution2(arr1, arr2, arr3);

        System.out.println("\nANSWER:");
        for(int i : intersection)
            System.out.print(i + ", ");
    }

    static LinkedList<Integer> solution(int[]... A) {

        System.out.println("INPUT:");
        for(int[] a : A) {
            for(int x : a)
                System.out.print(x + ", ");
            System.out.println();
        }

        System.out.println("INDEXES:");
        LinkedList<Integer> intersection = new LinkedList<>();
        int[] P = new int[A.length];  // pointers
        while(true) {

            // can be done without this loop; see solution 2
            boolean equal = true;
            for(int i = 0; i < P.length - 1; i++) {
                if(A[i][P[i]] != A[i+1][P[i+1]]) {
                    equal = false;
                    break;
                }
            }

            int smallest = Integer.MAX_VALUE;
            int s = 0;

            if(!equal) {
                for (int i = 0; i < P.length; i++) {
                    if (P[i] < A[i].length && A[i][P[i]] < smallest) {
                        smallest = A[i][P[i]];
                        s = i;
                    }
                }
                P[s] += 1;
                if(P[s] >= A[s].length)
                    break;  // stopping condition
            } else {
                intersection.add(A[0][P[0]]);
                for(int i = 0; i < P.length; i++) {
                    P[i] += 1;
                    if(P[i] >= A[s].length)
                        break;  // other stopping condition
                }
            }

            for(int p : P)
                System.out.print(p + ", ");
            System.out.println();
        }

        return intersection;
    }

    static LinkedList<Integer> solution2(int[]... A) {

        System.out.println("INPUT:");
        for(int[] a : A) {
            for(int x : a)
                System.out.print(x + ", ");
            System.out.println();
        }

        System.out.println("INDEXES:");
        LinkedList<Integer> intersection = new LinkedList<>();
        int[] P = new int[A.length];  // pointers
        while(true) {

            int smallest = A[0][P[0]];
            int s = 0;
            boolean equals = true;
            for (int i = 1; i < P.length; i++) {
                if (P[i] < A[i].length && A[i][P[i]] < smallest) {
                    smallest = A[i][P[i]];
                    s = i;
                    equals = false;
                } else if(P[i] < A[i].length && A[i][P[i]] > smallest) {
                    equals = false;
                }
            }

            if(!equals) {
                P[s] += 1;
                if (P[s] >= A[s].length)
                    break;  // stopping condition
            } else {
                intersection.add(A[0][P[0]]);
                for (int i = 0; i < P.length; i++) {
                    P[i] += 1;
                    if (P[i] >= A[s].length)
                        break;  // other stopping condition
                }
            }


            for(int p : P)
                System.out.print(p + ", ");
            System.out.println();
        }

        return intersection;
    }
}
