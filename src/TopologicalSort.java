import java.util.*;

/*
    Topological sort can be solved with two methods: Khan's algorithm and Depth-First-Search.
    A Directed Acyclic Graph (DAG) is required for all topological sorts.
    No solution can be found if a cycle is found in the graph.

    Khan's algorithm is a Decrease-and-Counter algorithm

    Source: https://en.wikipedia.org/wiki/Topological_sorting
    Source: "Introduction to the Design & Analysis of Algorithms", by Anany Levitin
 */

public class TopologicalSort {
    public static void main(String[] args) {

        // Input Source: "Introduction to the Design & Analysis of Algorithms", page 171
        String[] input = new String[]{"C1", "C2", "C3", "C4", "C5"};
        String[][] D = new String[][]{
                new String[]{input[0], input[2]},
                new String[]{input[1], input[2]},
                new String[]{input[2], input[3]},
                new String[]{input[2], input[4]},
                new String[]{input[3], input[4]}
        };

        // represents input as adjacency matrix
        /*int[][] G = new int[][] {
                new int[]{0, 0, 1, 0, 0},
                new int[]{0, 0, 1, 0, 0},
                new int[]{0, 0, 0, 1, 1},
                new int[]{0, 0, 0, 0, 1},
                new int[]{0, 0, 0, 0, 0},
        };*/

        int[][] G = buildGraph(input, D);

        LinkedList<Integer> path = dfs(G);
        printSolution(G, path, input);

        int[][] G2 = new int[][] {
                new int[]{0, 1, 0},
                new int[]{1, 0, 0},
                new int[]{1, 0, 0},
        };

        path = khan(G2);
        printSolution(G2, path, input);

        // Input Source: https://en.wikipedia.org/wiki/Topological_sorting
        // Multiple solutions exist
        input = new String[]{"2", "3", "5", "7", "8", "9", "10", "11"};
        D = new String[][]{
                new String[]{"5", "11"},
                new String[]{"11", "2"},
                new String[]{"7", "11"},
                new String[]{"7", "8"},
                new String[]{"8", "9"},
                new String[]{"11", "9"},
                new String[]{"3", "8"},
                new String[]{"11", "10"},
        };

        int[][] G3 = buildGraph(input, D);
        path = dfs(G3);
        printSolution(G3, path, input);
    }

    static int[][] buildGraph(String[] input, String[][] D) {
        int[][] G = new int[input.length][input.length];
        HashMap<String, Integer> map = new HashMap<>();
        for(int i = 0; i < input.length; i++)
            map.put(input[i], i);

        for(String[] d : D) {
            G[map.get(d[0])][map.get(d[1])] = 1;
        }

        return G;
    }

    // khan's algorithm
    static LinkedList<Integer> khan(int[][] input) {

        // copy array
        int[][] G = new int[input.length][input[0].length];
        for(int i = 0; i < G.length; i++)
            G[i] = Arrays.copyOf(input[i], input[i].length);

        LinkedList<Integer> L = new LinkedList<>(); // sorted list to return
        Queue<Integer> S = new LinkedList<>(); // start nodes

        for(int i = 0; i < G.length; i++)
            findNodesWithNoIncomingEdges(G, S, i);

        while(!S.isEmpty()) {
            int n = S.poll();
            L.addLast(n);

            for(int i = 0; i < G[n].length; i++) {
                if(G[n][i] > 0) {
                    G[n][i] = 0;  // remove edge
                    findNodesWithNoIncomingEdges(G, S, i);
                }
            }
        }

        if(L.size() != G.length)
            return null;  // contains cycle, thus no solution

        return L;
    }

    static LinkedList<Integer> dfs(int[][] G) {
        LinkedList<Integer> path = new LinkedList<>();
        boolean[] recStack = new boolean[G.length];

        Queue<Integer> S = new LinkedList<>();
        for(int i = 0; i < G.length; i++)
            findNodesWithNoIncomingEdges(G, S, i);

        boolean hasCycle = false;
        for(int start : S) {
            hasCycle = hasCycle || dfsRecursive(G, path, recStack, start);
            System.out.println("Has Cycle? " + hasCycle);
            if (hasCycle)
                return null;  // contains cycle, thus no solution
        }

        return path;
    }

    static boolean dfsRecursive(int[][] G, LinkedList<Integer> path, boolean[] recStack, int curr) {
        recStack[curr] = true;

        for(int i = 0; i < G[curr].length; i++) {
            if (G[curr][i] > 0) {
                if(recStack[i])
                    return true;
                if(dfsRecursive(G, path, recStack, i))
                    return true;
            }
        }

        if(!path.contains(curr)) {
            path.addFirst(curr);
            System.out.println("CURR: " + curr);
        }

        recStack[curr] = false;
        return false;
    }

    private static void findNodesWithNoIncomingEdges(int[][] G, Queue<Integer> S, int i) {
        boolean noEdges = true;
        for (int j = 0; j < G[0].length; j++) {
            if (G[j][i] == 1) {
                noEdges = false;
                break;
            }
        }

        if (noEdges)
            S.add(i);
    }

    static void printSolution(int[][] G, LinkedList<Integer> path) {
        printSolution(G, path, null);
    }

    static void printSolution(int[][] G, LinkedList<Integer> path, String[] input) {

        if(input == null) {
            input = new String[G.length];
            for(int i = 0; i < G.length; i++)
                input[i] = i + "";
        }

        System.out.println();

        for(int[] g : G) {
            for(int x : g)
                System.out.print(x);
            System.out.println();
        }

        if(path != null) {
            System.out.print("PATH: ");
            for (int x : path)
                System.out.print(input[x] + ",");
            System.out.println();
        } else
            System.out.println("NO SOLUTION!");

        System.out.println();
    }
}
