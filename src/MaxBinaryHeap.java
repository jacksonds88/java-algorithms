import java.util.ArrayList;
import java.util.LinkedList;

public class MaxBinaryHeap {

    public ArrayList<Integer> heap = new ArrayList<>();

    public void insert(int ... keys) {
        for(int key : keys) {
            heap.add(key);
            bubbleUp();
        }
    }

    public void deleteLast() {
        if(heap.isEmpty())
            return;

        maxHeapify(heap.remove(heap.size() - 1));
    }

    public void deleteMax() {
        if(heap.isEmpty())
            return;

        swap(0, heap.size() - 1);
        heap.remove(heap.size() - 1);
        maxHeapify(0);
    }

    public boolean isEmpty() {
        return heap.isEmpty();
    }

    public int extractMax() {
        int max = heap.get(0);

        swap(0, heap.size() - 1);
        heap.remove(heap.size() - 1);
        maxHeapify(0);

        return max;
    }

    public int getMax() {
        return heap.get(0);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(heap.size() * 2);
        final String COMMA = ", ";

        for(int key : heap)
            sb.append(key).append(COMMA);

        String heapString = sb.toString();
        if(heapString.length() > 0)
            return heapString.substring(0, heapString.length() - COMMA.length());
        else
            return heapString;
    }

    public String toString2() {
        StringBuilder sb = new StringBuilder(heap.size() * 2);
        final String COMMA = ", ";
        int level = 0;
        int capacity = (int) Math.pow(2, level);

        // 1, 2, 4, 8
        for(int i = 0; i < heap.size(); i++) {
            if(capacity == i + 1) {
                level++;
                capacity += (int)Math.pow(2, level);
                sb.append(heap.get(i)).append("\n");
            } else
                sb.append(heap.get(i)).append(COMMA);
        }

        // remove comma
        String heapString = sb.toString();
        if(heapString.length() > 0)
            return heapString.substring(0, heapString.length() - COMMA.length());
        else
            return heapString;
    }

    public static LinkedList<Integer> heapSort(MaxBinaryHeap heap) {
        LinkedList<Integer> sortedNumbers = new LinkedList<>();
        while(!heap.isEmpty())
            sortedNumbers.addFirst(heap.extractMax());

        return sortedNumbers;
    }

    // used for insert(), node must be appended to the end
    private void bubbleUp() {
        int current = heap.size() - 1;
        int parent = getParentIndex(current);

        while (heap.get(current) > heap.get(parent)) {
            swap(current, parent);
            current = parent;
            parent = getParentIndex(current);
        }
    }

    // re-arranges moving down the tree (the opposite of bubbleUp() method"
    private void maxHeapify(int i){
        int left = getLeftChild(i);  // 1 (89) / 3 (36)
        int right = getRightChild(i); // 2 (72) / 4 (75)
        int largest = i; // 0 (63) / 1 (63)

        if(left < heap.size() && // makes sure a left leaf exists
                heap.get(left) > heap.get(largest))
            largest = left;
        if(right < heap.size() &&
                heap.get(right) > heap.get(largest))
            largest = right;

        if(largest != i) {  // if current node is larger than child nodes, exit recursion
            swap(i, largest); // 0 (63), 1 (89) / 1 (63), 4 (75)
            maxHeapify(largest);  // 1, 4 ... moves down the sub heap
        }
    }

    private void swap(int i, int j) {
        int temp = heap.get(i);
        heap.set(i, heap.get(j));
        heap.set(j, temp);
    }

    // index starts from 0
    private static int getParentIndex(int index) {
        return (index-1) / 2;
    }

    private static int getLeftChild(int index) {
        return index * 2 + 1;
    }

    private static int getRightChild(int index) {
        return index * 2 + 2;
    }
}
