// Backtracking

import java.util.Stack;

public class RatInMaze {

    public static void main(String[] args) {
        char[][] M = new char[][]{
                new char[]{'_', '#', '#', 'S'},
                new char[]{'_', '_', '_', '_'},
                new char[]{'#', '_', '#', '#'},
                new char[]{'_', '_', '_', 'E'}
        };

        printMaze(M);
        State start = findStart(M);
        if(start != null) {
            System.out.println(solutionIterative(M, start.i, start.j));
            printMaze(M);
        }
    }

    static State findStart(char[][] M) {
        for(int i = 0 ; i < M.length; i++)
            for(int j = 0; j < M[i].length; j++)
                if(M[i][j] == 'S')
                    return new State(i, j);

        return null;
    }

    static boolean outOfBounds(char[][] M, int i, int j) {
        return i < 0 || i >= M.length || j < 0 || j >= M[i].length || M[i][j] == '#' || M[i][j] == '*';
    }

    static boolean solution(char[][] M, int i, int j) {
        if(M[i][j] == 'E')
            return true;

        M[i][j] = '*';
        boolean pathFound = false;

        printMaze(M);
        System.out.println();

        if(!outOfBounds(M, i+1, j))
            pathFound = solution(M, i+1, j);
        if(!outOfBounds(M, i, j+1) && !pathFound)
            pathFound = solution(M, i, j+1);
        if(!outOfBounds(M, i-1, j) && !pathFound)
            pathFound = solution(M, i-1, j);
        if(!outOfBounds(M, i, j-1) && !pathFound)
            pathFound = solution(M, i, j-1);

        if(!pathFound)
            M[i][j] = '_';

        return pathFound;
    }

    static void printMaze(char[][] M) {
        for(char[] m : M) {
            for(char c : m)
                System.out.print(c);
            System.out.println();
        }
    }

    static boolean solutionIterative(char[][] M, int i, int j) {
        Stack<State> stack = new Stack<>();
        stack.add(new State(i, j));

        while(!stack.isEmpty()) {
            State current = stack.peek();
            i = current.i;
            j = current.j;

            if (M[i][j] == 'E')
                return true;

            M[i][j] = '*';

            printMaze(M);
            System.out.println();

            if (!outOfBounds(M, i + 1, j)) {
                stack.push(new State(i + 1, j));
                continue;
            } if (!outOfBounds(M, i, j + 1)) {
                stack.push(new State(i, j + 1));
                continue;
            } if (!outOfBounds(M, i - 1, j)) {
                stack.push(new State(i - 1, j));
                continue;
            } if (!outOfBounds(M, i, j - 1)) {
                stack.push(new State(i, j - 1));
                continue;
            }

            M[i][j] = '_';
            stack.pop();
        }

        return false;
    }

    static class State {
        int i;
        int j;

        public State(int i, int j) {
            this.i = i;
            this.j = j;
        }
    }
}
