import java.util.Arrays;

public class Mergesort {
    public static void main(String[] args) {
        int[] A = new int[]{8, 3, 2, 9, 7, 1, 5, 4};
        int[] B = new int[]{8, 3, 2, 9, 7, 1, 5, 4, 6};

        mergesort(B);

        for(int x : B)
            System.out.print(x + ", ");
        System.out.println();
    }

    static void mergesort(int[] A) {
        if(A.length <= 1)
            return;

        int m = A.length / 2;
        int[] B = new int[m];
        int[] C = new int[A.length - m];

        copy(A, B, 0, m);
        copy(A, C, m, A.length);
        mergesort(B);
        mergesort(C);
        merge(A, B, C);
    }

    static void merge(int[] A, int[] B, int[] C) {
        int i = 0;
        int j = 0;
        int k = 0;

        while(j < B.length && k < C.length) {
            if(B[j] <= C[k]) {
                A[i++] = B[j++];
            } else {
                A[i++] = C[k++];
            }
        }

        while(j < B.length)
            A[i++] = B[j++];

        while(k < C.length)
            A[i++] = C[k++];
    }

    static void copy(int[] A, int[] B, int start, int length) {
        int c = 0;
        for(int i = start; i < length; i++) {
            B[c++] = A[i];
        }
    }

    // less efficient copy
    static void copy2(int[] A, int[] B, int start, int length) {
        for(int i = 0; i < length - start; i++) {
            B[i] = A[i + start];
        }
    }
}
