import java.util.HashSet;

// Source: https://youtu.be/GJdiM-muYqc

public class FirstRecurringCharacter {
    public static void main(String[] args) {
        String input = "DBCABA"; // return B
        String input2 = "ABC"; // return null
        String input3 = "ABCA"; // return A
        String input4 = "BCABA"; // return B

        System.out.println(solution2(input)); // B
        System.out.println(solution2(input2)); // null
        System.out.println(solution2(input3)); // A
        System.out.println(solution2(input4)); // B

        System.out.println();
        System.out.println(firstNonRecurringCharacter(input));  // D
        System.out.println(firstNonRecurringCharacter(input2));  // A
        System.out.println(firstNonRecurringCharacter(input4));  // C
        System.out.println(firstNonRecurringCharacterBruteForce(input));  // D
        System.out.println(firstNonRecurringCharacterBruteForce(input2));  // A
        System.out.println(firstNonRecurringCharacterBruteForce(input4));  // C

    }

    // big O(n), worst case is if there are no recurring characters
    static Character solution(String input) {
        HashSet<Character> map = new HashSet<>();
        for(char c : input.toCharArray())
            if(map.contains(c))
                return c;
            else
                map.add(c);

        return null;
    }

    // big O(n), worst case is if there are no recurring characters
    static Character solution2(String input) {
        boolean[] map = new boolean[128];  // ASCII size, 0 starts at 48, z ends at 122
        for(char c : input.toCharArray())
            if(map[c])
                return c;
            else
                map[c] = true;

        return null;
    }

    // big O(n^2)
    static Character solutionBruteForce(String input) {
        for(int i = 0; i < input.length() - 1; i++)
            for(int j = i + 1; j < input.length(); j++)
                if(input.charAt(i) == input.charAt(j))
                    return input.charAt(i);

        return null;
    }

    static Character firstNonRecurringCharacter(String input) {
        int[] map = new int[128];
        for(char c : input.toCharArray())
            map[c] += 1;

        for(char c : input.toCharArray())
            if(map[c] == 1)
                return c;

        return null;
    }

    static Character firstNonRecurringCharacterBruteForce(String input) {
        int[] map = new int[128];
        for(int i = 0; i < input.length() - 1; i++) {
            boolean nonRecurring = true;
            for (int j = i + 1; j < input.length(); j++)
                if(input.charAt(i) == input.charAt(j))
                    nonRecurring = false;

            if(nonRecurring)
                return input.charAt(i);
        }

        return null;
    }
}
