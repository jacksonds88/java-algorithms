import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;

public class Kruskals {
    public static void main(String[] args) {
        int[][] M = new int[6][6];
        M[0][1] = 7;
        M[0][2] = 9;
        M[0][5] = 14;

        M[1][0] = 7;
        M[1][2] = 10;
        M[1][3] = 15;

        M[2][0] = 9;
        M[2][1] = 10;
        M[2][3] = 11;
        M[2][5] = 2;

        M[3][1] = 15;
        M[3][1] = 11;
        M[3][3] = 6;

        M[4][3] = 6;
        M[4][5] = 9;

        M[5][0] = 14;
        M[5][2] = 2;
        M[5][4] = 9;

        /*LinkedList<Edge> edges = solution(M);

        // 0 1 2 5 4 3
        for(Edge edge : edges)
            System.out.println(edge);*/

        // source: https://www.tutorialspoint.com/data_structures_algorithms/prims_spanning_tree_algorithm.htm
        M = new int[6][6];
        String[] names = new String[]{"S", "A", "C", "B", "D", "T"};
        M[0][1] = 7;
        M[0][2] = 8;

        M[1][0] = 7;
        M[1][2] = 3;
        M[1][3] = 6;

        M[2][0] = 8;
        M[2][1] = 3;
        M[2][3] = 4;
        M[2][4] = 3;

        M[3][1] = 6;
        M[3][2] = 4;
        M[3][4] = 2;
        M[3][5] = 5;

        M[4][2] = 3;
        M[4][3] = 2;
        M[4][5] = 2;

        M[5][3] = 5;
        M[5][4] = 2;

        LinkedList<Edge> edges = solution(M);

        // 0 1 2 5 4 3
        for(Edge edge : edges)
            System.out.println(Edge.prettyPrint(edge, names));
    }

    static LinkedList<Edge> solution(int[][] M) {
        ArrayList<Edge> edges = new ArrayList<>(M.length * M.length);
        for(int i = 0; i < M.length; i++)
            for(int j = i + 1; j < M[i].length; j++)
                if(M[i][j] > 0)
                    edges.add(new Edge(M[i][j], i, j));

        Collections.sort(edges);

        // create N sets where each set has just one vertex
        HashSet<HashSet<Integer>> C = new HashSet<>(); // to check for cycles
        for(int i = 0; i < M.length; i++) {
            HashSet<Integer> c = new HashSet<>();
            c.add(i);
            C.add(c);
        }

        LinkedList<Edge> MST = new LinkedList<>();
        int count = 0;
        for(Edge edge : edges) {
            if(C.size() == 1)
                break; // all sets unioned into one

            if(count < M.length - 1) {
                HashSet<Integer> A = findSet(edge.a, C);
                HashSet<Integer> B = findSet(edge.b, C);

                //if(!isSubset(A, B) && !isSubset(B, A)) {
                //if(!intersects(A, B)) {
                if(!A.contains(edge.b) || !B.contains(edge.a)) {
                    C.add(union(A, B));
                    MST.add(edge);
                    count++;
                }
            }
        }

        for(HashSet<Integer> H : C)
            for(Integer c : H)
                System.out.print(c + ", ");
        System.out.println();

        return MST;
    }

    static HashSet<Integer> findSet(int x, HashSet<HashSet<Integer>> sets) {
        for(HashSet<Integer> s : sets)
            if(s.contains(x))
                return s;

        return null;
    }

    static HashSet<Integer> union(HashSet<Integer> A, HashSet<Integer> B) {
        HashSet<Integer> C = new HashSet<>();
        C.addAll(A);
        C.addAll(B);
        A.clear();
        B.clear();
        return C;
    }

    static boolean equals(HashSet<Integer> A, HashSet<Integer> B) {
        return isSubset(A, B) && isSubset(B, A);
    }

    static boolean intersects(HashSet<Integer> A, HashSet<Integer> B) {
        HashSet<Integer> smaller;
        HashSet<Integer> bigger;

        if(A.size() < B.size()) {
            smaller = A;
            bigger = B;
        } else {
            smaller = B;
            bigger = A;
        }

        for(int i : smaller)
            if(bigger.contains(i))
                return true;

        return false;
    }

    // A is subset of B ?
    static boolean isSubset(HashSet<Integer> A, HashSet<Integer> B) {
        for(int i : A)
            if(!B.contains(i))
                return false;

        return true;
    }

    static class Edge implements Comparable<Edge> {

        int distance;
        int a;
        int b;

        public Edge(int distance, int a, int b) {
            this.distance = distance;
            this.a = a;
            this.b = b;
        }

        @Override
        public int compareTo(Edge edge) {
            if(this.distance == edge.distance)
                return 0;
            else if(this.distance < edge.distance)
                return -1;
            else
                return 1;
        }

        @Override
        public boolean equals(Object o) {
            Edge other = (Edge) o;

            return (this.a == other.a && this.b == other.b) || (this.a == other.b && this.b == other.a);
        }

        @Override
        public String toString() {
            return "a=" + a + ", b=" + b + ": distance=" + distance;
        }

        static String prettyPrint(Edge edge, String[] names) {
            return names[edge.a] + "," + names[edge.b] + ": " + edge.distance;
        }
    }
}
