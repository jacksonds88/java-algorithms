public class EuclideanAlgorithm {
    public static void main(String[] args) {
        System.out.println("gcdByDiv: " + gcdByDiv(21, 7));
        System.out.println("gcdByDiv: " + gcdByDiv(7, 21));
        System.out.println("gcdByDiv: " + gcdByDiv(9, 5));

        System.out.println("gcdBySubtraction: " + gcdBySubtraction(21, 7));
        System.out.println("gcdBySubtraction: " + gcdBySubtraction(7, 21));
        System.out.println("gcdBySubtraction: " + gcdBySubtraction(9, 5));

        System.out.println("gcdRecursion: " + gcdRecursion(21, 7));
        System.out.println("gcdRecursion: " + gcdRecursion(7, 21));
        System.out.println("gcdRecursion: " + gcdRecursion(9, 5));
    }

    static int gcdByDiv(int a, int b) {
        while (b != 0) {
            int t = b;
            System.out.println(a + " % " + b + " = " + (a%b));
            b = a % b;
            a = t;
        }
        return a;
    }

    static int gcdBySubtraction(int a, int b) {
        while(a != b) {
            if(a > b)
                a = a - b;
            else
                b = b - a;
        }

        return a;
    }

    static int gcdRecursion(int a, int b) {
        if(b == 0)
            return a;
        else
            return gcdRecursion(b, a % b);
    }
}
