// Hamiltonian Cycle is a backtracking problem

import java.util.HashSet;

public class HamiltonianCycle {
    public static void main(String[] args) {
        // input from "Introduction to the Design and Analysis of Algorithms", page 371 (Backtracking)

        int[][] G1 = new int[][]{
                new int[]{0, 1, 1, 1, 0, 0},
                new int[]{1, 0, 1, 0, 0, 1},
                new int[]{1, 1, 0, 1, 1, 0},
                new int[]{1, 0, 1, 0, 1, 0},
                new int[]{0, 0, 1, 1, 0, 1},
                new int[]{0, 1, 0, 0, 1, 0}
        };

        // true
        System.out.println(isHamiltonCycle(G1, 0, 0, new HashSet<>()));

        int[][] G2 = new int[][]{
                new int[]{0, 1, 1, 1, 1},
                new int[]{1, 0, 1, 0, 0},
                new int[]{1, 1, 0, 0, 0},
                new int[]{1, 0, 0, 0, 1},
                new int[]{1, 0, 0, 1, 0}
        };

        // false
        System.out.println(isHamiltonCycle(G2, 0, 0, new HashSet<>()));
    }

    static boolean isHamiltonCycle(int[][] G, int start, int curr, HashSet<Integer> V) {
        V.add(curr);

        if(G.length == V.size())
            return G[curr][start] > 0;

        boolean ret = false;
        for(int next = 0; next < G[curr].length; next++)
            if(G[curr][next] > 0 && !V.contains(next))
                ret = ret || isHamiltonCycle(G, start, next, V);

        V.remove(curr);
        return ret;
    }
}
