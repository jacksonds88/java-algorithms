
public class Quicksort {
    public static void main(String[] args) {
        int[] A = {5, 4, 3, 6, 1, 9, 8, 2, 7};
        quicksort(A, 0, A.length);

        for(int x : A)
            System.out.print(x);
        System.out.println();

        A = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
        //quicksort(A, 0, A.length);

        for(int x : A)
            System.out.print(x);
        System.out.println();
    }

    static void quicksort(int[] A, int lo, int hi) {
        if(hi - lo > 1) {

            System.out.println();
            int p = partition(A, lo, hi);
            System.out.println("p=" + p + ": lo=" + lo + " to hi=" + hi);
            System.out.println();

            quicksort(A, lo, p); // hi is exclusive!
            quicksort(A, p + 1, hi);
        }
    }

    // Hoare partition scheme
    static int partition(int[] A, int lo, int hi) {
        System.out.println("PARTITION");

        for(int a = lo; a < hi; a++)
            System.out.print(A[a]);
        System.out.println();

        int mid = (hi - lo) / 2 + lo;
        System.out.println("mid index: " + mid);
        if(A[lo] > A[mid])
            swap(A, lo, mid);
        if(A[mid] > A[hi-1])
            swap(A, mid, hi-1);
        if(A[lo] < A[mid])
            swap(A, lo, mid);

        System.out.println("Median-of-Three applied");
        for(int a = lo; a < hi; a++)
            System.out.print(A[a]);
        System.out.println();

        int pivot = A[lo];
        int i = lo + 1;
        int j = hi - 1;

        while(true) {
            System.out.println("A[i=" + i +"] < pivot: " + A[i] + " < " + pivot);
            while(i < hi && A[i] < pivot)
                i++;
            System.out.println("i: " + i);

            System.out.println("A[j=" + j +"] > pivot: " + A[j] + " > " + pivot);
            while(j > lo && A[j] > pivot)
                j--;
            System.out.println("j: " + j);

            if(i > j) {
                System.out.println("swap pivot[" + lo + "," + j + "]: " + A[lo] + " and " + A[j]);
                swap(A, lo, j);
                return j;
            } else if(i == j)
                return j;

            System.out.println("swap normal[" + lo + "," + j + "]: " + A[lo] + " and " + A[j]);
            swap(A, i, j);
        }
    }

    static void swap(int[] A, int i, int j) {
        int temp = A[i];
        A[i] = A[j];
        A[j] = temp;
    }
}
