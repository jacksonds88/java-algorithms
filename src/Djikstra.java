import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;

/*
    Source: https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm
 */

public class Djikstra {
    public static void main(String[] args) {
        // input from https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm
        int[][] M = new int[][] {
                new int[]{0, 7, 9, 0, 0, 14},
                new int[]{7, 0, 10, 15, 0, 0},
                new int[]{9, 10, 0, 11, 0, 2},
                new int[]{0, 15, 11, 0, 6, 0},
                new int[]{0, 0, 0, 6, 0, 9},
                new int[]{14, 0, 2, 0, 9, 0}
        };

        int[][] M2 = new int[][] {
                new int[]{0, 3, 1, 0, 0},
                new int[]{3, 0, 1, 2, 5},
                new int[]{1, 1, 0, 4, 0},
                new int[]{0, 2, 4, 0, 1},
                new int[]{0, 5, 0, 1, 0},
        };


        int[] path = djikstra(M2, 0, 4);
        for(int v : path)
            System.out.print(v);
        System.out.println();
    }

    // https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm
    static int[] djikstra(int[][] M, int start, int end) {
        int[] path = new int[M.length];
        HashSet<Integer> Q = new HashSet<>();
        int[] distance = new int[M.length];
        for(int i = 0; i < distance.length; i++) {
            distance[i] = Integer.MAX_VALUE;
            Q.add(i);
            path[i] = -1;  // unvisited
        }

        Q.add(start);
        distance[start] = 0;
        path[start] = 0;

        while (!Q.isEmpty()) {
            Integer curr = getVertexWithLeastDistance(Q, distance);
            if(curr == end) {
                System.out.println();
                System.out.print("DISTANCE: ");
                for(int x : distance)
                    System.out.print(x + ", ");
                System.out.println();

                return path;
            }

            Q.remove(curr);
            System.out.println("curr:  " + curr);

            for(int i = 0; i < M[curr].length; i++)
                if(M[curr][i] > 0) {
                    int d = distance[curr] + M[curr][i];
                    System.out.println(i + ": " + distance[curr] + " + " + M[curr][i] + " = " + d);
                    if(d < distance[i]) {
                        distance[i] = d;
                        path[i] = curr;
                    }
                }
        }

        return path;
    }

    static void djikstraWithoutTracers(int[][] M, int start, int end) {
        HashSet<Integer> Q = new HashSet<>();
        int[] distance = new int[M.length];
        for(int i = 0; i < distance.length; i++) {
            distance[i] = Integer.MAX_VALUE;
            Q.add(i);
        }

        Q.add(start);
        distance[start] = 0;

        while (!Q.isEmpty()) {
            Integer curr = getVertexWithLeastDistance(Q, distance);
            if(curr == end) return;
            Q.remove(curr);

            for(int i = 0; i < M[curr].length; i++)
                if(M[curr][i] > 0) {
                    int d = distance[curr] + M[curr][i];
                    if(d < distance[i])
                        distance[i] = d;
                }
        }
    }

    private static int getVertexWithLeastDistance(HashSet<Integer> Q, int[] distance) {
        int min = Integer.MAX_VALUE;
        int v = 0;
        for(int i = 0; i < distance.length; i++)
            if(Q.contains(i) && distance[i] != Integer.MAX_VALUE && distance[i] < min) {
                min = distance[i];
                v = i;
            }

        return v;
    }

    // DOES NOT WORK with M2 input
    static void djikstraNaive(int[][] M, int start, int end) {
        Queue<Integer> Q = new LinkedList<>();
        Q.add(start);
        boolean[] V = new boolean[M.length];
        int[] distance = new int[M.length];
        for(int i = 1; i < distance.length; i++)
            distance[i] = Integer.MAX_VALUE;

        while (!Q.isEmpty()) {
            int curr = Q.poll();
            V[curr] = true;
            if(curr == end) {
                for(int x : distance)
                    System.out.print(x + ", ");
                System.out.println();

                return;
            }

            for(int i = 0; i < M[curr].length; i++)
                if(M[curr][i] > 0 && !V[i]) {
                    int d = distance[curr] + M[curr][i];
                    if(d < distance[i])
                        distance[i] = d;

                    if(!Q.contains(i))
                        Q.add(i);
                }
        }
    }
}
