package tesla_question;

public class Dice {
    public static void main(String[] args) {
        int[] A1 = new int[]{1, 1, 6};  // 2
        int[] A2 = new int[]{1, 6, 2, 3};  // 4

        // System.out.println(solution(A1));
        System.out.println("Solution: " + solution2(A2));
    }

    static final int[] oppositeFaces = new int[]{6, 5, 4, 3, 2, 1};

    int faceUp;

    public Dice(int faceUp) {
        this.faceUp = faceUp;
    }

    int rotate(int newFaceUp) {
        if(oppositeFaces[faceUp - 1] == newFaceUp)
            return 2;
        else if(faceUp != newFaceUp)
            return 1;
        else
            return 0;
    }

    static int solution(int[] dieFaceUps) {
        if(dieFaceUps == null || dieFaceUps.length == 0)
            return 0;

        Dice[] die = new Dice[dieFaceUps.length];
        for(int i = 0; i < die.length; i++)
            die[i] = new Dice(dieFaceUps[i]);

        int[] tryFaces = new int[]{1, 2, 3, 4, 5, 6};
        int minSum = Integer.MAX_VALUE;
        for (int tryFace : tryFaces) {
            int sum = 0;
            for (Dice aDie : die) {
                System.out.print(aDie.rotate(tryFace) + ", ");
                sum += aDie.rotate(tryFace);
            }
            System.out.println();

            System.out.println(sum);
            if (minSum > sum)
                minSum = sum;
        }

        return minSum;
    }

    // without using a Dice object
    static int solution2(int[] dieFaceUps) {
        if(dieFaceUps == null || dieFaceUps.length == 0)
            return 0;

        int minSum = Integer.MAX_VALUE;
        for (int i = 0; i <= 6; i++) {  // i represents the dice pips (faces) that we are aiming for
            int sum = 0;

            for(int face : dieFaceUps) {
                int numRotations;
                if(oppositeFaces[face - 1] == i)
                    numRotations = 2;
                else if(face != i)
                    numRotations = 1;
                else
                    numRotations = 0;

                System.out.print(numRotations + ", ");
                sum += numRotations;
            }

            System.out.println();
            System.out.println(sum);
            if (minSum > sum)
                minSum = sum;
        }

        return minSum;
    }
}
