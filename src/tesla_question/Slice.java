package tesla_question;

import java.util.ArrayList;
import java.util.LinkedList;

public class Slice {
    public static void main(String[] args){
        System.out.println("Slice are " + bestSolution(new int[]{2, 4, 1, 6, 5, 9, 7})); //3
        System.out.println("Slice are " + bestSolution(new int[]{4, 3, 2, 6, 1})); //1
        System.out.println("Slice are " + bestSolution(new int[]{2, 1, 6, 4, 3, 7})); //3
        System.out.println("Slice are " + bestSolution(new int[]{1, 2, 3, 4})); //4
        System.out.println("Slice are " + bestSolution(new int[]{1, 2, 3, 4})); //4
        System.out.println("Slice are " + bestSolution(new int[]{4, 3, 2, 1})); //1
        System.out.println("Slice are " + bestSolution(new int[]{4, 3, 1, 2}));  //1
        System.out.println("Slice are " + bestSolution(new int[]{4})); // 1
        System.out.println("Slice are " + bestSolution(new int[]{})); // 0
        System.out.println("Slice are " + bestSolution(new int[]{4, 3, 2, 6, 5})); //2

        //System.out.println("Slice are " + solution2(new int[]{4, 3, 2, 6, 5})); // 2 (prints 1 which is wrong)
        //System.out.println("Slice are " + bestSolution(new int[]{4, 3, 2, 6, 5})); //2
    }

    // final int MAX_VALUE = 1,000,000,000 for each element
    static final int MAX_N = 100000;
    int max;
    int min;
    LinkedList<Integer> numbers;

    public Slice(int x) {
        this.max = x;
        this.min = x;
        numbers = new LinkedList<>();
        numbers.add(x);
    }

    void insert(int x) {
        if(x > max)
            max = x;
        else if (x < min)
            min = x;
        numbers.add(x);
    }

    void merge(Slice S) {
        while(!S.numbers.isEmpty())
            this.insert(S.numbers.removeFirst());
    }

    public static int solution(int[] A) {
        if(A == null || A.length == 0)
            return 0;

        ArrayList<Slice> slices = new ArrayList<>(A.length);
        slices.add(new Slice(A[0]));

        for(int i = 1; i < A.length;i++) {
            Slice currSlice = slices.get(slices.size() - 1);

            if(A[i] > currSlice.max)
                slices.add(new Slice(A[i]));
            else {
                currSlice.insert(A[i]);
                int j = slices.size() - 1;
                while(j > 0) {
                    if (slices.get(j).min < slices.get(j-1).max) {
                        slices.get(j-1).merge(slices.get(j));
                        slices.remove(j);
                        j--;
                    } else
                        break;
                }
            }
        }

        System.out.println("----------------");
        for(Slice s : slices) {
            for (int x : s.numbers)
                System.out.print(x + ", ");
            System.out.println();
        }

        return slices.size();
    }

    // no objects used
    // numbers are not preserved for optimal performance
    public static int bestSolution(int[] A) {
        if(A == null || A.length == 0)
            return 0;

        ArrayList<int[]> S = new ArrayList<>(A.length);
        S.add(new int[]{A[0], A[0]});
        final int MIN = 0;
        final int MAX = 1;

        for(int i = 1; i < A.length;i++) {
            int[] currSlice = S.get(S.size() - 1);

            if(A[i] > currSlice[MAX])
                S.add(new int[]{A[i], A[i]});
            else {
                if(A[i] < currSlice[MIN])
                    currSlice[MIN] = A[i];

                int j = S.size() - 1;
                while(j > 0) {
                    if (S.get(j)[MIN] < S.get(j-1)[MAX]) {

                        // merge last two slices (includes deleting one slice)
                        int max = (S.get(j-1)[MAX] > S.get(j)[MAX]) ? S.get(j-1)[MAX] : S.get(j)[MAX];
                        int min = (S.get(j-1)[MIN] < S.get(j)[MIN]) ? S.get(j-1)[MIN] : S.get(j)[MIN];
                        S.get(j-1)[MAX] = max;
                        S.get(j-1)[MIN] = min;

                        S.remove(j);
                        j--;
                    } else
                        break;
                }
            }
        }

        return S.size();
    }

    // brian solution
    // DOES NOT WORK
    public static int solution2(int[] A) {
        if(A == null || A.length == 0)
            return 0;

        int count = 0;
        int previousMax = 0;
        int currentMax = 0;
        boolean reset = true;
        for (int i = 0; i < A.length - 1; i++) {
            if (reset) {
                currentMax = A[i];
                reset = false;
            }
            if (currentMax < A[i + 1]) {
                count++;
                reset = true;
            }

            if (currentMax < previousMax) {
                count = 1;
                previousMax = currentMax;
            }
        }

        if (A[A.length - 1] > currentMax) {
            count++;
        }

        return Math.max(count, 1);
    }
}
