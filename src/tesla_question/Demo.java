package tesla_question;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;

/*
    Find the smallest positive integer NOT in the input list. If no positive integers exist, return 1.
 */

public class Demo {
    public static void main(String[] args) {
        int[] A = new int[]{1, 3, 6, 4, 1, 2};
        int[] B = new int[]{1, 2, 3};
        int[] C = new int[]{-1, -3};

        System.out.println("SOLUTION2: " + solution2(A));
        System.out.println("SOLUTION2: " + solution2(B));
        System.out.println("SOLUTION2: " + solution2(C));

        System.out.println("SOLUTION3: " + solution3(A));
        System.out.println("SOLUTION3: " + solution3(B));
        System.out.println("SOLUTION3: " + solution3(C));
    }

    // time complexity is O(3N) which is O(N)
    // space complexity is O(2N) which is O(N)
    static int solution(int[] A) {
        int positiveMin;
        HashSet<Integer> S = new HashSet<>();

        for(int x : A)
            if(x > 0)
                S.add(x + 1);

        for(int x : A)
            S.remove(x);

        if(S.iterator().hasNext())
            positiveMin = S.iterator().next();
        else
            return 1;

        for(int x : S)
            if(x < positiveMin)
                positiveMin = x;

        return positiveMin;
    }

    // time complexity is O(3N) which is O(N)
    // space complexity is O(2N) which is O(N)
    static int solution2(int[] A) {
        HashSet<Integer> S = new HashSet<>();

        for(int x : A)
            if(x > 0)
                S.add(x + 1);

        for(int x : A)
            S.remove(x);

        return (S.isEmpty()) ? 1 : Collections.min(S);
    }

    // time complexity is O(N log N)
    // solution is in-place
    static int solution3(int[] A) {
        Arrays.sort(A);

        for(int i = 0; i < A.length-1; i++) {
            if(A[i] > 0 && A[i+1] - A[i] > 1)
                return A[i]+1;
        }

        return A[A.length-1] > 0 ? A[A.length-1] + 1 : 1;
    }

}
