public class Fraction {
    public static void main(String[] args) {

        // 0.0/0.0 54.0/252.0 36.0/252.0 324.0/504.0

        /*System.out.println(asFraction(500, 1000)); //  "1/2"
        System.out.println(asFraction(17, 3));     //  "17/3"
        System.out.println(asFraction(462, 1071)); //  "22/51"

        Fraction[] F = new Fraction[4];
        F[0] = new Fraction(54, 252);
        F[1] = new Fraction(36, 252);
        F[2] = new Fraction(324, 504);
        F[3] = new Fraction(0, 0);

        solution(F);

        for(Fraction f : F)
            System.out.println(f);
        System.out.println();*/

        Fraction[] F = new Fraction[4];
        F[0] = new Fraction(1, 7);
        F[1] = new Fraction(1, 14);
        F[2] = new Fraction(9, 14);
        F[3] = new Fraction(1, 1);


        solution(F);

        for(Fraction f : F)
            System.out.println(f);
    }

    public static String asFraction(long a, long b) {
        long gcm = gcm(a, b);
        return (a / gcm) + "/" + (b / gcm);
    }

    public static long gcm(long a, long b) {
        return b == 0 ? a : gcm(b, a % b); // Not bad for one line of code :)
    }

    static void solution(Fraction[] F) {
        int gcd = gcd(F);

        System.out.println("gcd: " + gcd);

        for(Fraction f : F)
            f.scale(gcd);


        for(Fraction f : F)
            System.out.println(f);

        int min = F[0].d;
        for(int i = 1; i < F.length; i++) {
            if(F[i].d < min && F[i].d != 0)
                min = F[i].d;
        }

        System.out.println("min: " + min);

        for(int i = 1; i < F.length; i++) {
            if(F[i].d != min && F[i].n % (F[i].d / min) == 0) {
                F[i].divide(F[i].d / min);
            }
        }

        System.out.println(gcd);
    }

    static int gcd(Fraction[] F) {
        int gcd = F[0].d;

        for(int i = 1; i < F.length; i++) {
            int temp = gcd(gcd, F[i].d);

            if (temp != 1) {
                gcd = temp;
            }
        }

        return gcd;
    }


    static int gcd(int a, int b) {
        if(a == 0)
            return b;

        while(b != 0) {
            int t = b;
            b = a % b;
            a = t;
        }

        return a;
    }

    int n;
    int d;

    public Fraction(int n, int d) {
        this.n = n;
        this.d = d;
    }

    void scale(int s) {
        n *= s;
        d *= s;
    }

    void divide(int s) {
        if(n != 0) n /= s;
        if(d != 0) d /= s;
    }

    public String toString() {
        return n + "/" + d;
    }
}
