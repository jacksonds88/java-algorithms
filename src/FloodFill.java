
// Source: https://youtu.be/IWvbPIYQPFM
// Find the count of connected colors in a table. The solution requires a Floodfill algorithm.
// you can only traverse up, down, left, right

import java.util.LinkedList;
import java.util.Queue;

public class FloodFill {

    static final int BLUE = 0;
    static final int BLACK = 1;
    static final int RED = 2;

    public static void main(String[] args) {
        int[][] T = new int[3][4];
        boolean[][] V = new boolean[T.length][T[0].length]; // visited
        T[0][0] = BLUE;
        T[0][1] = BLUE;
        T[0][2] = BLACK;
        T[0][3] = RED;

        T[1][0] = BLUE;
        T[1][1] = BLACK;
        T[1][2] = RED;
        T[1][3] = BLACK;

        T[2][0] = RED;
        T[2][1] = BLACK;
        T[2][2] = BLACK;
        T[2][3] = BLACK;

        //System.out.println(breadthFirstSearch(T, V, 0, 0));
        //System.out.println(depthFirstSearch(T, V, 2, 3, 0));
        System.out.println(solution(T, V));
    }

    // floodfill
    static int solution(int[][] T, boolean[][] V) {
        int max = 0;
        for(int i = 0; i < T.length; i++)
            for(int j = 0; j < T[i].length; j++) {
                int size = breadthFirstSearch(T, V, i, j);
                //int size = depthFirstSearch(T, V, i, j, 0);
                if(size > max)
                    max = size;
            }

        return max;
    }

    static int breadthFirstSearch(int[][] T, boolean[][] V, int i, int j) {

        Queue<String> Q = new LinkedList<>();
        int color = T[i][j];
        int count = 0;
        Q.add(i + "" + j);
        while(!Q.isEmpty()) {
            String s = Q.poll();
            i = Character.getNumericValue(s.charAt(0));
            j = Character.getNumericValue(s.charAt(1));

            V[i][j] = true;
            count++;

            if(notOutOfBounds(T, i+1, j) && T[i+1][j] == color && !V[i+1][j])
                Q.add((i+1) + "" + j);
            if(notOutOfBounds(T, i, j+1) && T[i][j+1] == color && !V[i][j+1])
                Q.add(i + "" + (j+1));
            if(notOutOfBounds(T, i-1, j) && T[i-1][j] == color && !V[i-1][j])
                Q.add((i-1) + "" + j);
            if(notOutOfBounds(T, i, j-1) && T[i][j-1] == color && !V[i][j-1])
                Q.add(i + "" + (j-1));
        }

        return count;
    }

    static int depthFirstSearch(int[][] T, boolean[][] V, int i, int j, int count) {
        int color = T[i][j];
        V[i][j] = true;

        if(notOutOfBounds(T, i+1, j) && T[i+1][j] == color && !V[i+1][j])
            count = depthFirstSearch(T, V, (i + 1), j, count);

        if(notOutOfBounds(T, i, j+1) && T[i][j+1] == color && !V[i][j+1])
            count = depthFirstSearch(T, V, i, j + 1, count);

        if(notOutOfBounds(T, i-1, j) && T[i-1][j] == color && !V[i-1][j])
            count = depthFirstSearch(T, V, i - 1, j, count);

        if(notOutOfBounds(T, i, j-1) && T[i][j-1] == color && !V[i][j-1])
            count = depthFirstSearch(T, V, i, j - 1, count);

        System.out.println(i + "," + j + ": " + count);
        return ++count;
    }

    static boolean notOutOfBounds(int[][] T, int i, int j) {
        return (i >= 0 && i < T.length && j >= 0 && j < T[0].length);
    }
}
