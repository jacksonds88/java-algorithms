package knapsack;

/*
  This is called the 0/1 Knapsack Dynamic Programming, where 0/1 means we either take an item or we do not.
  Secondly, the weights and the capacity (totalWeight) are positive integers

  Source: https://youtu.be/8LusJS5-AGo
  2nd Source: https://www.youtube.com/watch?v=nLmhmB6NzcM
 */

public class KnapsackDynamicProgramming {
    public static void main(String[] args) {
        int totalWeight = 7;
        int n = 4;
        int[] val = new int[n];
        val[0] = 1;
        val[1] = 4;
        val[2] = 5;
        val[3] = 7;

        int[] wt = new int[n];
        wt[0] = 1;
        wt[1] = 3;
        wt[2] = 4;
        wt[3] = 5;

        System.out.println(solution(totalWeight, val, wt));
    }

    static int solution(int totalWeight, int[] val, int[] wt) {
        int[][] T = new int[wt.length + 1][totalWeight + 1];
        // the first entire row and first entire column should be all zeros
        printTable(T);

        for(int i = 1; i < T.length; i++) {
            for(int j = 1; j < T[i].length; j++)  // j is capacity
                if(j < wt[i-1])
                    T[i][j] = T[i-1][j];
                else
                    T[i][j] = Math.max(val[i-1] + T[i - 1][j - wt[i-1]], T[i - 1][j]);
        }

        System.out.println();
        printTable(T);


        return T[T.length-1][totalWeight];
    }

    static void printTable(int[][] T) {
        for(int i = 0; i < T.length; i++) {
            for(int j = 0; j < T[i].length; j++)
                System.out.print(T[i][j]);
            System.out.println();
        }
    }
}
