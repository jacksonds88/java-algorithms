package knapsack;

// Knapsack greedy
// Source: https://youtu.be/8LusJS5-AGo?t=43

import java.util.ArrayList;
import java.util.Collections;

public class KnapsackGreedy {
    public static void main(String[] args) {
        double capacity = 7;
        int n = 4;
        double[] val = new double[n];
        val[0] = 1;
        val[1] = 4;
        val[2] = 5;
        val[3] = 7;

        double[] wt = new double[n];
        wt[0] = 1;
        wt[1] = 3;
        wt[2] = 4;
        wt[3] = 5;

        // System.out.println(solution(capacity, val, wt));
        System.out.println(solution3(capacity, val, wt));
    }

    // sort by MaxBinaryHeap
    static double solution(double capacity, double[] val, double[] wt) {
        Item[] items = buildItems(val, wt);
        MaxBinaryHeap maxBinaryHeap = new MaxBinaryHeap(val.length);

        for(Item item : items)
            maxBinaryHeap.insert(item);

        double totalWeight = 0;
        double maxValue = 0;
        for(int i = 0; i < items.length; i++) {
            Item item = maxBinaryHeap.pop();
            System.out.println(item.toString());
            if(totalWeight + item.weight <= capacity) {
                totalWeight += item.weight;
                maxValue += item.value;
            } else {
                double remainingWeight = capacity - totalWeight;
                double scaledValue = (remainingWeight / item.weight) * item.value;
                System.out.println("Scaled item: " + item);
                System.out.println("Scaled Value: " + scaledValue);
                maxValue += scaledValue;
                break;
            }
        }

        return maxValue;
    }

    // sort by Java's quicksort using the Comparable interface
    static double solution2(double capacity, double[] val, double[] wt) {
        ArrayList<Item> items = buildItemsAsList(val, wt);
        Collections.sort(items);

        double totalWeight = 0;
        double maxValue = 0;
        for(Item item : items) {
            System.out.println(item.toString());
            if(totalWeight + item.weight <= capacity) {
                totalWeight += item.weight;
                maxValue += item.value;
            } else {
                double remainingWeight = capacity - totalWeight;
                double scaledValue = (remainingWeight / item.weight) * item.value;
                System.out.println("Scaled item: " + item);
                System.out.println("Scaled Value: " + scaledValue);
                maxValue += scaledValue;
                break;
            }
        }

        return maxValue;
    }

    // sort by bubble sort
    static double solution3(double capacity, double[] val, double[] wt) {
        Item[] items = buildItems(val, wt);
        bubbleSort(items);

        double totalWeight = 0;
        double maxValue = 0;
        for(Item item : items) {
            System.out.println(item.toString());
            if(totalWeight + item.weight <= capacity) {
                totalWeight += item.weight;
                maxValue += item.value;
            } else {
                double remainingWeight = capacity - totalWeight;
                double scaledValue = (remainingWeight / item.weight) * item.value;
                System.out.println("Scaled item: " + item);
                System.out.println("Scaled Value: " + scaledValue);
                maxValue += scaledValue;
                break;
            }
        }

        return maxValue;
    }

    static Item[] buildItems(double[] val, double[] wt) {
        Item[] items = new Item[val.length];
        for(int i = 0; i < val.length; i++)
            items[i] = new Item(i, val[i], wt[i]);
        return items;
    }

    static ArrayList<Item> buildItemsAsList(double[] val, double[] wt) {
        ArrayList<Item> items = new ArrayList<>(val.length);
        for(int i = 0; i < val.length; i++)
            items.add(new Item(i, val[i], wt[i]));
        return items;
    }

    static void bubbleSort(Item[] items) {
        boolean swapped = false;
        for(int n = items.length; n > 1; n--) {
            for (int i = 0; i < n - 1; i++)
                if (items[i].worth < items[i + 1].worth) {
                    Item temp = items[i];
                    items[i] = items[i + 1];
                    items[i + 1] = temp;
                    swapped = true;
                }

            if(!swapped)
                break;
            swapped = false;
        }
    }

    static class Item implements Comparable<Item>  {

        int index;
        double value;
        double weight;
        double worth;

        public Item(int index, double value, double weight) {
            this.index = index;
            this.value = value;
            this.weight = weight;
            worth = value / weight;
        }

        public String toString() {
            return "(" + index + ": " + value + " / " + weight + " = " + worth + ")";
        }

        public int compareTo(Item other) {
            if(this.worth > other.worth)
                return -1;
            else if(this.worth < other.worth)
                return 1;
            else
                return 0;
        }
    }

    static class MaxBinaryHeap {

        Item[] heap;
        int size;

        public MaxBinaryHeap(int size) {
            heap = new Item[size];
        }

        public void insert(Item item) {
            heap[size] = item;
            int i = size;
            size++;

            // bubble-up
            int parent = getParentIndex(i);
            while(heap[parent].worth < heap[i].worth) {

                int leftChild = getLeftChildIndex(parent);
                int rightChild = getRightChildIndex(parent);

                int largest;  // index
                if(rightChild >= size || heap[leftChild].worth > heap[rightChild].worth)
                    largest = leftChild;
                else
                    largest = rightChild;

                swap(parent, largest);
                i = parent;
                parent = getParentIndex(i);
            }
        }

        public Item pop() {
            if(size == 0)
                return null;

            Item ret = heap[0];
            size--;
            heap[0] = heap[size];
            heap[size] = null;

            // bubble-down
            int parent = 0;
            int leftChild = 1;
            int rightChild = 2;
            while((leftChild < size && heap[parent].worth < heap[leftChild].worth) ||
                    (rightChild < size && heap[parent].worth < heap[rightChild].worth)) {

                if(rightChild >= size || heap[leftChild].worth > heap[rightChild].worth) {
                    swap(leftChild, parent);
                    parent = leftChild;
                    leftChild = getLeftChildIndex(parent);
                    rightChild = getRightChildIndex(parent);
                    if(leftChild > size)
                        break;

                } else {
                    swap(rightChild, parent);
                    parent = rightChild;
                    leftChild = getLeftChildIndex(parent);
                    rightChild = getRightChildIndex(parent);
                    if(rightChild > size)
                        break;
                }

            }

            return ret;
        }

        void swap(int i, int j) {
            Item temp = heap[i];
            heap[i] = heap[j];
            heap[j] = temp;
        }

        int getLeftChildIndex(int i) {
            return i * 2 + 1;
        }

        int getRightChildIndex(int i) {
            return i * 2 + 2;
        }

        int getParentIndex(int i) {
            return (i - 1) / 2;
        }
    }
}
