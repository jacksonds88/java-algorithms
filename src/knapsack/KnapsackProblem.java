package knapsack;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;

public class KnapsackProblem {
    public static void main(String[] args) {

        HashSet<Item> items = new HashSet<>();
        /*items.add(new Item("green", 12d, 4d));
        items.add(new Item("gray", 1d, 2d));
        items.add(new Item("blue", 2d, 2d));
        items.add(new Item("orange", 1d, 1d));
        items.add(new Item("yellow", 4d, 10d));*/
        items.add(new Item("0", 60d, 10d));
        items.add(new Item("1", 100d, 20d));
        items.add(new Item("2", 120d, 30d));
        double W = 50d; // capacity

        HashSet<HashSet<Item>> P = new HashSet<>();
        P.add(items);
        P = getPowerSet(P, items);

        /*if(P != null)
            for(HashSet<Item> subset : P) {
                for (Item item : subset)
                    System.out.print(item);
                System.out.println();
            }*/

        if(P != null)
            knapsackBruteForce(W, P);  // solution is 220

        knapsackPowerSetIntegrated(W, items);
    }

    // function f(S)
    // if P(S) is empty
    //   return {{}}
    // else
    //   for each e in S
    //   T = S / e
    //   return P(S) = T U f(T)
    static HashSet<HashSet<Item>> getPowerSet(HashSet<HashSet<Item>> P, HashSet<Item> S) {
        if(S.isEmpty())
            return null;

        for(Item item : S) {
            HashSet<Item> T = new HashSet<>(S);
            T.remove(item);

            if(!P.contains(T)) {
                P.add(T);
                getPowerSet(P, T);
            }
        }

        return P;
    }

    static double knapsackBruteForce(double capacity, HashSet<HashSet<Item>> P) {
        System.out.println("knapsack brute force");

        double maxValue = 0d;
        HashSet<Item> bestCatch = null;

        for(HashSet<Item> S : P) {
            printHashSet(S);

            double totalWeight = 0d;
            double totalValue = 0d;
            for(Item item : S) {
                if(totalWeight + item.weight <= capacity) {
                    totalWeight += item.weight;
                    totalValue += item.value;
                    if(totalValue > maxValue) {
                        maxValue = totalValue;
                        bestCatch = S;
                    }
                }
            }
        }

        System.out.println("BEST CATCH: " + maxValue);
        if(bestCatch != null)
            printHashSet(bestCatch);

        return maxValue;
    }

    static double knapsackPowerSetIntegrated(double capacity, HashSet<Item> items) {
        System.out.println("\n\nknapsack power set integrated");
        HashSet<Item> bestCatch = items;
        double totalWeight = 0d;
        double totalValue = 0d;
        boolean allItemsFit = true;
        for (Item item : items) {
            if (totalWeight + item.weight <= capacity) {
                totalWeight += item.weight;
                totalValue += item.weight;
            } else {
                allItemsFit = false;
                break;
            }
        }

        if(allItemsFit) {
            System.out.println("BEST CATCH: " + totalValue);
            printHashSet(bestCatch);
            return totalValue;
        }

        double maxValue = 0d;
        bestCatch = null;

        Queue<HashSet<Item>> queue = new LinkedList<>();
        queue.add(items);

        while(!queue.isEmpty()) {
            HashSet<Item> S = queue.poll();

            for(Item item : S) {
                HashSet<Item> T = new HashSet<>(S);
                T.remove(item);
                printHashSet(T);

                totalWeight = 0d;
                totalValue = 0d;
                double lastMaxValue = maxValue;
                HashSet<Item> lastBestCatch = bestCatch;

                for (Item item2 : T) {
                    if (totalWeight + item2.weight <= capacity) {
                        totalWeight += item2.weight;
                        totalValue += item2.value;
                        if (totalValue > maxValue) {
                            maxValue = totalValue;
                            bestCatch = T;
                        }
                    } else {
                        maxValue = lastMaxValue;
                        bestCatch = lastBestCatch;
                        queue.add(T);
                        break;
                    }
                }
            }
        }

        System.out.println("BEST CATCH: " + maxValue);
        if(bestCatch != null)
            printHashSet(bestCatch);

        return maxValue;
    }

    static void printHashSet(HashSet<Item> items) {
        for(Item i : items)
            System.out.print(i);
        System.out.println();
    }

    static class Item {
        String id;
        double value, weight;

        public Item(String id, double value, double weight) {
            this.id = id;
            this.value = value;
            this.weight = weight;
        }

        @Override
        public String toString() {
            return "(" + id + ", " + value + ", " + weight + ")";
            //return id.substring(0, 1).toUpperCase();
        }
    }
}
