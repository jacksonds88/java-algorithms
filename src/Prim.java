import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;

public class Prim {
    public static void main(String[] args) {

        int[][] M = new int[][] {
                new int[]{0, 7, 9, 0, 0, 14},
                new int[]{7, 0, 10, 15, 0, 0},
                new int[]{9, 10, 0, 11, 0, 2},
                new int[]{0, 15, 11, 0, 6, 0},
                new int[]{0, 0, 0, 6, 0, 9},
                new int[]{14, 0, 2, 0, 9, 0}
        };

        int[][] M2 = new int[][] {
                new int[]{0, 3, 1, 0, 0},
                new int[]{3, 0, 1, 2, 5},
                new int[]{1, 1, 0, 4, 0},
                new int[]{0, 2, 4, 0, 1},
                new int[]{0, 5, 0, 1, 0},
        };

        //breadthFirstSearch(M, 4);
        //System.out.println();

        System.exit(0);

        // source: https://www.tutorialspoint.com/data_structures_algorithms/prims_spanning_tree_algorithm.htm
        M = new int[6][6];
        String[] names = new String[]{"S", "A", "C", "B", "D", "T"};
        M[0][1] = 7;
        M[0][2] = 8;

        M[1][0] = 7;
        M[1][2] = 3;
        M[1][3] = 6;

        M[2][0] = 8;
        M[2][1] = 3;
        M[2][3] = 4;
        M[2][4] = 3;

        M[3][1] = 6;
        M[3][2] = 4;
        M[3][4] = 2;
        M[3][5] = 5;

        M[4][2] = 3;
        M[4][3] = 2;
        M[4][5] = 2;

        M[5][3] = 5;
        M[5][4] = 2;

        LinkedList<String> MST = prim(M, names);
        // primWithMinHeap doesn't work yet

        // correct output: SA AC CD DB DT
        for(String s : MST)
            System.out.print(s + " ");
        System.out.println();
    }

    static void breadthFirstSearch(int[][] M, int end) {
        Queue<Integer> Q = new LinkedList<>();
        Q.add(0);
        boolean[] V = new boolean[M.length];

        while (!Q.isEmpty()) {
            int curr = Q.poll();
            V[curr] = true;
            System.out.println(curr);

            for(int i = 0; i < M[curr].length; i++)
                if(M[curr][i] > 0 && !V[i] && !Q.contains(i)) {
                    Q.add(i);
                }
        }
    }

    static LinkedList<String> prim(int[][] M, String[] names) {
        LinkedList<String> MST = new LinkedList<>();
        HashSet<Integer> V = new HashSet<>();
        V.add(0);

        while(V.size() != M.length) { // loop of visited nodes

            int minDistance = Integer.MAX_VALUE;
            int closestVisitedVertex = -1;
            int closestUnvisitedVertex = -1;
            for(int v : V)
                for (int j = 0; j < M[v].length; j++) { // loop of unvisited nodes
                    if (M[v][j] > 0 && M[v][j] < minDistance && !V.contains(j)) {
                        minDistance = M[v][j];
                        closestUnvisitedVertex = j;
                        closestVisitedVertex = v;
                    }
                }

            MST.add(names[closestVisitedVertex] + names[closestUnvisitedVertex]);
            V.add(closestUnvisitedVertex);
        }

        return MST;
    }

    static LinkedList<String> primWithMinHeap(int[][] M, String[] names) {
        LinkedList<String> MST = new LinkedList<>();
        HashSet<Integer> V = new HashSet<>();
        MinHeap U = new MinHeap(M.length);

        V.add(0);
        for (int j = 0; j < M[0].length; j++) { // loop of unvisited nodes
            if (M[0][j] > 0 && !V.contains(j)) {
                U.insert(new Vertex(j, M[0][j], 0));
            }
        }


        while(V.size() != M.length) { // loop of visited nodes
            Vertex current = U.pop();
            V.add(current.to);

            for (int j = 0; j < M[0].length; j++) { // loop of unvisited nodes
                if (M[current.to][j] > 0 && !V.contains(j)) {
                    U.insert(new Vertex(j, M[current.to][j], current.to));
                }
            }

            MST.add(names[current.from] + names[current.to]);
            V.add(current.to);
        }

        return MST;
    }

    static class Vertex {
        int to;
        int distance;
        int from;

        public Vertex(int to, int distance, int from) {
            this.to = to;
            this.distance = distance;
            this.from = from;
        }

        @Override
        public String toString() {
            return to + "," + distance;
        }
    }

    static class MinHeap {
        Vertex[] H;
        HashSet<Integer> E;  // to prevent duplicates
        int size;

        public MinHeap(int size) {
            H = new Vertex[size];
        }

        public void insert(Vertex x) {
            if(E.contains(x.to))
                return;
            else
                E.add(x.to);


            H[size] = x;
            size++;

            for(int i = size; i > 1; i = i /2) {
                int p = (i - 1) / 2;
                if(H[i-1].distance < H[p].distance)
                    swap(i-1, p);
                else
                    break;
            }
        }

        public Vertex pop() {
            if(size <= 0)
                return null;
            size--;
            Vertex ret = H[0];
            int i = 0;
            swap(i, size);
            H[size] = null;


            int l = 2*i + 1;
            int r = 2*i + 2;

            while(i < size - 1) {
                int bigger = i;
                if(l < size && H[bigger].distance > H[l].distance)
                    bigger = l;
                if(r < size && H[bigger].distance > H[r].distance)
                    bigger = r;

                if(H[i] == H[bigger])
                    break;
                else
                    swap(i, bigger);

                i = bigger;
                l = 2*i + 1;
                r = 2*i + 2;
            }

            return ret;
        }

        void swap(int i, int j) {
            Vertex temp = H[i];
            H[i] = H[j];
            H[j] = temp;
        }
    }
}
