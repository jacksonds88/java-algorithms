import java.util.LinkedList;

public class App {
    public static void main(String[] args) {

        // testMaxBinaryHeap();

        BinarySearchTree binaryTree = new BinarySearchTree();

        binaryTree.insert(2);
        binaryTree.insert(3);
        binaryTree.insert(1);
        binaryTree.insert(3);
        binaryTree.insert(4);
    }

    static void testBinarySearchTree() {
        BinarySearchTree binaryTree = new BinarySearchTree();

        binaryTree.insert(2);
        binaryTree.insert(3);
        binaryTree.insert(1);
        binaryTree.insert(3);
        binaryTree.insert(4);
        //binaryTree.insert(5,"e");


        System.out.println(binaryTree.search(4));

        System.out.println("\n" + binaryTree);

        binaryTree.traverseInOrder();
        System.out.println();
        binaryTree.traversePreOrder();
        System.out.println();
        binaryTree.traversePostOrder();
    }

    static void testMaxBinaryHeap() {
        MaxBinaryHeap heap = new MaxBinaryHeap();
        heap.insert(90, 89, 72, 36, 75, 70, 65, 21, 18, 15, 12, 63, 70);
        System.out.println(heap.toString2());

        /*
            90
            89, 72
            36, 75, 70, 65
            21, 18, 15, 12, 63, 70
         */

        heap.deleteLast();
        System.out.println(heap.toString2());

        /*
            90
            89, 72
            36, 75, 70, 65
            21, 18, 15, 12, 63
         */

        heap.deleteMax();
        System.out.println(heap.toString2());

        /*
            89
            75, 72
            36, 63, 70, 65
            21, 18, 15, 12
         */

        System.out.println(heap.getMax());

        LinkedList<Integer> sortedNumbers = MaxBinaryHeap.heapSort(heap);
        for(int x : sortedNumbers)
            System.out.print(x + " ");
    }
}


/*
>> Writing out the recursion from the BST
insert(2);
root = insert(2, root);  // null, just return

insert(3);
insert(3, root)  // 1 > 2
root.right = insert(3, root.right)  // null, just return
root = insert(3, root)


insert(1);
insert(1, root)  // 1 < 2
root.left = insert(1, root.left)
root = insert(1, root)

insert(4);
insert(4, root)  // 4 > 2
insert(4, root.right)  // 4 > 3
root.right.right = insert(4, root.right.right)  // null, just return
root.right = insert(4, root.right)
root = insert(4, root)

 */