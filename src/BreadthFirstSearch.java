/*

// python code
A = array('I', [0, 1, 2, 3, 4, 5])  # A, B, C, D, E, F

#adjancey matrix
M = [[0 for x in range(len(A))] for y in range(len(A))]

# A to B,C,D
M[0][1] = 7
M[0][2] = 9
M[0][5] = 14

# B to A,C,D
M[1][0] = 7
M[1][2] = 10
M[1][3] = 15

M[2][0] = 9
M[2][1] = 10
M[2][3] = 11
M[2][5] = 2

M[3][1] = 15
M[3][1] = 11
M[3][3] = 6

M[4][3] = 6
M[4][5] = 9

M[5][0] = 14
M[5][2] = 2
M[5][4] = 9
 */

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;

public class BreadthFirstSearch {
    public static void main(String[] args) {
        String[] N = new String[]{"A", "B", "C", "D", "E", "F"};
        int[][] M = new int[6][6];
        M[0][1] = 7;
        M[0][2] = 9;
        M[0][5] = 14;

        M[1][0] = 7;
        M[1][2] = 10;
        M[1][3] = 15;

        M[2][0] = 9;
        M[2][1] = 10;
        M[2][3] = 11;
        M[2][5] = 2;

        M[3][1] = 15;
        M[3][1] = 11;
        M[3][3] = 6;

        M[4][3] = 6;
        M[4][5] = 9;

        M[5][0] = 14;
        M[5][2] = 2;
        M[5][4] = 9;

        LinkedList<String> T = solution2(N, M);

        /*
            Solution should be:
            0: A
            1: B
            2: C
            5: F
            3: D
            4: E
         */

        for(String s : T)
            System.out.print(s);
    }

    static LinkedList<String> solution(String[] N, int[][] M) {
        boolean[] V = new boolean[N.length];  // visited

        LinkedList<String> T = new LinkedList<>(); // the order of nodes traversed
        Queue<Integer> Q = new LinkedList<>();
        Q.add(0); // 0 -> A
        while(!Q.isEmpty()) {
            int current = Q.poll();
            V[current] = true;
            T.add(N[current]);
            System.out.println(current + ": " + N[current]);

            for(int i = 0; i < M[current].length; i++)
                if(M[current][i] > 0 && !V[i] && !Q.contains(i))
                    Q.add(i);
        }

        return T;
    }

    // use the help of a hashset for quicker lookup (instead of !Q.contains(current))
    static LinkedList<String> solution2(String[] N, int[][] M) {
        boolean[] V = new boolean[N.length];  // visited

        LinkedList<String> T = new LinkedList<>(); // the order of nodes traversed
        Queue<Integer> Q = new LinkedList<>();
        HashSet<Integer> inQ = new HashSet<>();
        Q.add(0); // 0 -> A
        while(!Q.isEmpty()) {
            int current = Q.poll();
            inQ.remove(current);
            V[current] = true;
            T.add(N[current]);
            System.out.println(current + ": " + N[current]);

            for(int i = 0; i < M[current].length; i++)
                if(M[current][i] > 0 && !V[i] && !inQ.contains(i)) {
                    Q.add(i);
                    inQ.add(i);
                }
        }

        return T;
    }
}
