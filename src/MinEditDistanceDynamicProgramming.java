
/*
Minimum Edit Distance Dynamic Programming

Source: https://www.youtube.com/watch?v=We3YDTzNXEk

If two characters are same, you don't do anything, that's clear right? If not, then there's 3 possible moves: delete,
replace and insert.

Suppose your rows represents str1, cols represents str2. If you wanna delete, you go back to previous column, which
is T[i][j-1], because you'd like to retrieve the last character in str2. Similarly, if you wanna insert, you go back
to previous row, which is T[i-1][j]. For replacing current character, you need to retrieve last character in both str1
and str2, which is T[i-1][j-1]. At last, you get the minimum among the above 3 moves and plus 1 to it to get T[i][j].
 */

public class MinEditDistanceDynamicProgramming {
    public static void main(String[] args) {
        String input = "abcdef";
        String target = "azced";

        System.out.println(solution(input, target));
    }

    /*
        Given input "abcdef" and a target of "azced", the dynamic table should look like this:

        0123456
        1012345
        2112345
        3221234
        4332223
        5443233

        The answer is the bottom-most right position, i.e., T[m][n] where n is input length and m is target length
     */
    static int solution(String input, String target) {
        int[][] table = new int[target.length() + 1][input.length() + 1];

        // the first row and first column should increment from 0 to n
        for(int i = 0; i < table.length; i++)
            table[i][0] = i;

        for(int i = 0; i < table[0].length; i++)
            table[0][i] = i;

        int[] adjacent = new int[3];
        for(int row = 1; row < table.length; row++) {
            for(int col = 1; col < table[row].length; col++) {
                // get smallest from left, above, and diaganolly in-between
                adjacent[0] = table[row][col-1]; // left (delete)
                adjacent[1] = table[row-1][col]; // above (insert)
                adjacent[2] = table[row-1][col-1];  // diaganolly (replace)
                int smallest = adjacent[0];
                for(int i = 1; i < adjacent.length; i++)
                    if(smallest > adjacent[i])
                        smallest = adjacent[i];

                if(target.charAt(row-1) == input.charAt(col-1))
                    table[row][col] = smallest;
                else
                    table[row][col] = smallest + 1;
            }
        }

        for(int[] row : table) {
            for (int x : row)
                System.out.print(x);
            System.out.println();
        }

        return table[target.length()][input.length()];
    }
}
