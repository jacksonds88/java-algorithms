public class SelectionSort {
    public static void main(String[] args) {
        int[] numbers = {5, 4, 3, 6, 1};
        sort(numbers);

        for(int x : numbers)
            System.out.print(x + ", ");
    }

    static void sort(int[] numbers) {
        for(int i = 0; i < numbers.length - 1; i++) {
            for(int j = i + 1; j < numbers.length; j++) {
                if(numbers[i] > numbers[j])
                    swap(numbers, i, j);
            }
        }
    }

    static void swap(int[] numbers, int i, int j) {
        int temp = numbers[i];
        numbers[i] = numbers[j];
        numbers[j] = temp;
    }
}
