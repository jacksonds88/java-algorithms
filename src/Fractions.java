public class Fractions {
    public static void main(String[] args) {
        // 1/7, 1/14, 2/21, 1/1
        Fraction[] F = new Fraction[4];
        F[0] = new Fraction(1, 7);
        F[1] = new Fraction(2, 14);
        F[2] = new Fraction(9, 21);
        F[3] = new Fraction(2, 2);

        solution(F);

        for(Fraction f : F)
            System.out.println(f);
    }

    static void solution(Fraction[] F) {
        int gcd = gcd(F);

        for(Fraction f : F)
            if(f.d != gcd)
                f.scale(gcd);

        int min = F[0].d;
        for(int i = 1; i < F.length; i++) {
            if(F[i].d < min)
                min = F[i].d;
        }

        for(int i = 1; i < F.length; i++) {
            if(F[i].d != min) {
                F[i].divide(F[i].d / min);
            }
        }

        System.out.println(gcd);
    }

    static int gcd(Fraction[] F) {
        int gcd = F[0].d;

        for(int i = 1; i < F.length; i++) {
            int temp = gcd(gcd, F[i].d);

            if (temp != 1) {
                gcd = temp;
            }
        }

        return gcd;
    }


    static int gcd(int a, int b) {
        while(b != 0) {
            int t = b;
            b = a % b;
            a = t;
        }

        return a;
    }

    static class Fraction {
        int n;
        int d;

        public Fraction(int n, int d) {
            this.n = n;
            this.d = d;
        }

        void scale(int s) {
            n *= s;
            if(d != 0)
                d *= s;
        }

        void divide(int s) {
            n /= s;
            if(d != 0)
                d /= s;
        }

        public String toString() {
            return n + "/" + d;
        }
    }
}
