import java.util.*;

public class BinarySearchTree {

    public static void main(String[] args) {
        BinarySearchTree binaryTree = new BinarySearchTree();

        binaryTree.insert(2);
        binaryTree.insert(3);
        binaryTree.insert(1);
        binaryTree.insert(3);  // repeat, should be ignored
        binaryTree.insert(4);
        //binaryTree.insert(5,"e");


        System.out.println(binaryTree.search(4));

        System.out.println("\n" + binaryTree);

        System.out.println("In-Order Traversal");
        binaryTree.traverseInOrder();
        System.out.println();
        System.out.println("Pre-Order Traversal");
        binaryTree.traversePreOrder();
        System.out.println();
        System.out.println("Post-Order Traversal");
        binaryTree.traversePostOrder();
        System.out.println();


        System.out.println("Breadth First Search on Binary Tree");
        LinkedList<Node> T = binaryTree.breadthFirstSearch();
        for(Node n : T)
            System.out.println(n);

        binaryTree.insert(0);
        System.out.println("Depth First Search on Binary Tree");
        binaryTree.depthFirstSearchInOrderTraversal();

    }

    private class Node {

        int key;
        int level;
        Node left;
        Node right;

        public Node(int key, int level) {
            this.key = key;
            this.level = level;
        }

        public String toString() {
            return "(KEY="+key+",LEVEL="+level+")";
        }
    }

    private Node root;
    private int size;

    public BinarySearchTree() {

    }

    // more efficient than recursion
    public LinkedList<Node> breadthFirstSearch() {
        if(root == null)
            return new LinkedList<>();

        LinkedList<Node> T = new LinkedList<>(); // nodes traversed
        // visited is not needed because binary trees have no cycles
        //HashSet<Node> visited = new HashSet<>();
        Queue<Node> queue = new LinkedList<>();
        queue.add(root);

        while(!queue.isEmpty()) {
            Node current = queue.poll();
            //visited.add(current);
            T.add(current);

            // visited is not needed because binary trees have no cycles
            //if(!visited.contains(current.left) && !queue.contains(current.left))
            if(current.left != null)
                queue.add(current.left);
            if(current.right != null)
                queue.add(current.right);
        }

        return T;
    }

    public void depthFirstSearchInOrderTraversal() {
        if(root == null)
            return;

        HashSet<Node> visited = new HashSet<>();
        Stack<Node> stack = new Stack<>();
        Node current = root;
        stack.add(current);

        while(!stack.isEmpty()) {
            if(current.left != null && !visited.contains(current.left)) {
                stack.add(current.left);
                current = current.left;
                continue;  // keeps going all the way left
            }
            current = stack.pop();
            visited.add(current);
            System.out.println(current);
            if(current.right != null && !visited.contains(current.right)) {
                stack.add(current.right);
                current = current.right;
            }
        }
    }

    // Recurrence: T(n) = T(n/2) + 1
    // Order of Growth: O(log n)
    public void insert(int key) {
        root = insert(key, root, 0);
    }

    private Node insert(int key, Node node, int level) {
        if(node == null) {
            size++;
            return new Node(key, level);
        } else {
            if(key == node.key)
                System.out.println("Key already exists. Overwriting!");
            else if(key < node.key)
                node.left = insert(key, node.left, ++level);
            else //if(key > node.key)
                node.right = insert(key, node.right, ++level);
        }

        return node;
    }


    public Node search(int key) {
        return search(key, root);
    }

    private Node search(int key, Node node) {
        if(node == null) {
            System.out.println("Key does not exist!");
            return null;
        }

        if(key < node.key)
            return search(key, node.left);
        else if(key > node.key)
            return search(key, node.right);
        else
            return node;
    }

    public void traverseInOrder() {
        traverseInOrder(root);
    }

    // Inorder (left, root, right)
    private void traverseInOrder(Node node) {
        if(node == null)
            return;

        traverseInOrder(node.left);
        System.out.println(node.toString());
        traverseInOrder(node.right);
    }

    public void traversePreOrder() {
        traversePreOrder(root);
    }

    // Preorder (root, left, right)
    private void traversePreOrder(Node node) {
        if(node == null)
            return;
        System.out.println(node.toString());
        traversePreOrder(node.left);
        traversePreOrder(node.right);
    }

    public void traversePostOrder() {
        traversePostOrder(root);
    }

    // Postorder (left, right, root)
    private void traversePostOrder(Node node) {
        if(node == null)
            return;

        traversePostOrder(node.left);
        traversePostOrder(node.right);
        System.out.println(node.toString());
    }

    public int getSize() {
        return size;
    }

    public String toString() {
        HashMap<Integer, String> levels = new HashMap<>();

        LinkedList<Node> nodes = buildListFromTree();

        System.out.println(nodes.size());

        for(Node node : nodes)
            levels.put(node.level, levels.getOrDefault(node.level, "") + node.toString() + ",");

        StringBuilder sb = new StringBuilder();
        for(String level : levels.values()) {
            sb.append(level).append("\n");
        }

        return sb.toString();
    }

    private LinkedList<Node> buildListFromTree() {
        LinkedList<Node> nodes = new LinkedList<>();
        buildListFromTree(root, nodes);
        return nodes;
    }

    private void buildListFromTree(Node node, LinkedList<Node> nodes) {
        if(node == null)
            return;

        nodes.add(node);
        buildListFromTree(node.left, nodes);
        buildListFromTree(node.right, nodes);
    }
}
