import java.util.HashSet;
import java.util.LinkedList;
import java.util.Stack;

public class DepthFirstSearch {
    public static void main(String[] args) {
        String[] N = new String[]{"A", "B", "C", "D", "E", "F"};
        int[][] M = new int[6][6];
        M[0][1] = 7;
        M[0][2] = 9;
        M[0][5] = 14;

        M[1][0] = 7;
        M[1][2] = 10;
        M[1][3] = 15;

        M[2][0] = 9;
        M[2][1] = 10;
        M[2][3] = 11;
        M[2][5] = 2;

        M[3][1] = 15;
        M[3][1] = 11;
        M[3][3] = 6;

        M[4][3] = 6;
        M[4][5] = 9;

        M[5][0] = 14;
        M[5][2] = 2;
        M[5][4] = 9;

        LinkedList<String> T = solution(N, M);  // should give DEFCBA
        for(String s : T)
            System.out.print(s);
        System.out.println();

        System.out.println("Has cycle: " + hasCycle(M));


        // input from source: https://en.wikipedia.org/wiki/Depth-first_search
        N = new String[]{"A", "B", "C", "D", "E", "F", "G"};
        M = new int[7][7];
        // A to B, C, E
        M[0][1] = 1;
        M[0][2] = 1;
        M[0][4] = 1;

        // B to A, D, F
        M[1][0] = 1;
        M[1][3] = 1;
        M[1][5] = 1;

        // C to A, G
        M[2][0] = 1;
        M[2][6] = 1;

        // D to B
        M[3][1] = 1;

        // E to A, F
        M[4][0] = 1;
        M[4][5] = 1;

        // F to B, E
        M[5][1] = 1;
        M[5][4] = 1;

        // G to C
        M[6][2] = 1;

        T = solution(N, M); // should give DEFBGCA
        for(String s : T)
            System.out.print(s);
        System.out.println();

        System.out.println("Has cycle: " + hasCycle(M));

        N = new String[]{"A", "B", "C", "D"};
        M = new int[4][4];
        // A to B, C, D
        M[0][1] = 1;
        M[0][2] = 1;
        M[0][3] = 1;

        System.out.println("Has cycle: " + hasCycle(M));
    }

    static LinkedList<String> solution(String[] N, int[][] M) {
        LinkedList<String> T = new LinkedList<>();
        Stack<Integer> stack = new Stack<>();
        HashSet<Integer> V = new HashSet<>(); // visited
        stack.push(0);

        while(!stack.empty()) {
            int current = stack.peek();
            boolean deadEnd = true;
            for(int i = 0; i < M[current].length; i++) {
                if(M[current][i] > 0 && !V.contains(i) && !stack.contains(i)) {
                    stack.push(i);
                    deadEnd = false;
                    break;
                }
            }

            if(deadEnd) {
                int popped = stack.pop();
                T.add(N[popped]);
                V.add(popped); // will loop infinitely without a visited data-structure
            }
        }

        return T;
    }

    static boolean hasCycle(int[][] M) {
        HashSet<Integer> V = new HashSet<>();
        Stack<Integer> S = new Stack<>();
        int curr = 0;
        S.push(curr);

        //  while(S.peek() != null) { // throws java.util.EmptyStackException
        while(!S.isEmpty()) {
            for(int i = 0; i < M[curr].length; i++)
                if (M[curr][i] > 0)
                    if (V.contains(i))
                        return true;
                    else {
                        S.push(i);
                        curr = i;
                        break;  // break, not continue
                    }

            int popped = S.pop();
            V.add(popped);
        }

        return false;
    }
}
