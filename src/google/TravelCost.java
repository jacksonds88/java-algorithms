package google.interview;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;

public class TravelCost {
    public static void main(String[] args) {
        Integer[] C = {0, 6, 4, 7};
        int K = 5;
        HashSet<ArrayList<Integer>> S = getAllPathsDP(new ArrayList<>(Arrays.asList(C)), new HashSet<>());

        for(ArrayList<Integer> A : S) {
            for(Integer x : A)
                System.out.print(x + ", ");
            System.out.println();
        }

        System.out.println("Solution: " +  solution(S, K));
    }

    static int solution(HashSet<ArrayList<Integer>> S, int K) {
        int min = Integer.MAX_VALUE;

        int count = 0;
        for(ArrayList<Integer> A : S) {
            int cost = 0;
            for(int i = 0; i < A.size(); i++)
                cost += Math.pow(A.get(i) - K, 2);

            System.out.println("i=" + count + ": " + cost);
            count++;
            if(cost < min)
                min = cost;
        }

        return min;
    }

    static HashSet<ArrayList<Integer>> getAllPaths(ArrayList<Integer> A, HashSet<ArrayList<Integer>> S) {
        // MUST NOT CONTAIN THE EMPTY SET!
        if(A.isEmpty())
            return S;
        else
            S.add(A);

        LinkedList<Integer> toDelete = new LinkedList<>();
        for(Integer x : A) {
            toDelete.add(x);
            ArrayList<Integer> a = new ArrayList<>(A);
            for(Integer y : toDelete) {
                a.remove(y);
            }
            getAllPaths(a, S);
        }

        return S;
    }

    // dynamic programming
    static HashSet<ArrayList<Integer>> getAllPathsDP(ArrayList<Integer> A, HashSet<ArrayList<Integer>> S) {
        S.add(A);

        if(A.isEmpty())
            return S;

        LinkedList<Integer> toDelete = new LinkedList<>();
        for(Integer x : A) {
            toDelete.add(x);
            ArrayList<Integer> a = new ArrayList<>(A);
            for(Integer y : toDelete) {
                a.remove(y);
            }
            if(!S.contains(a))
                getAllPaths(a, S);
        }

        return S;
    }

    // DOES NOT WORK b/c primitive array types do not override the hashCode function. USE LISTS instead!
    // source: https://stackoverflow.com/questions/28344312/hashset-usage-with-int-arrays
    static HashSet<int[]> getAllPaths3(int[] A, HashSet<int[]> S) {
        S.add(A);

        if(A == null || A.length == 0)
            return S;

        for(int i = 1; i < A.length; i++) {
            int[] a = new int[A.length - i];
            for(int j = 0; j < a.length; j++)
                a[j] = A[i + j];

            if(!S.contains(a))
                getAllPaths3(a, S);
        }

        return S;
    }
}
