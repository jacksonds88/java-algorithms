package google.interview;

import java.util.HashSet;
import java.util.Stack;

public class RobotCleaner {

    private static final int[] ROW = {-1, 0, 1, 0}; // up, right, down, left
    private static final int[] COL = {0, 1, 0, -1}; // up, right, down, left

    public static void main(String[] args) {
        int[][] M1 = new int[][]{
                new int[]{0, 0, 0, 0, 0, 0},
                new int[]{0, 1, 0, 1, 1, 0},
                new int[]{0, 1, 0, 1, 1, 0},
                new int[]{1, 1, 0, 0, 0, 0},
                new int[]{0, 1, 0, 0, 0, 0},
                new int[]{0, 1, 0, 0, 0, 0}
        };

        int[][] M2 = new int[][]{new int[]{0}};

        solution(M1, 0, 0);
    }

    static void solution(int[][] M, int r, int c) {
        Stack<State> stack = new Stack<>();
        stack.push(new State(r, c));
        HashSet<State> V = new HashSet<>();
        Robot robot = new Robot(M, r, c);
        int dir = 0;

        while(!stack.isEmpty()) {

            State curr = stack.peek();
            V.add(curr);
            robot.clean();

            System.out.println();
            System.out.println("CURR: " + curr);
            for(State a : V)
                System.out.print(a + ", ");
            System.out.println();

            boolean push = false;
            for(int i = 0; i < 4; i++) {
                dir = (dir + 1) % 4;
                robot.turnRight(1);
                State next = new State(curr.r + ROW[dir], curr.c + COL[dir]);
                if(!outOfBounds(M, next.r, next.c) && !V.contains(next)) {
                    boolean moved = robot.move();
                    System.out.println("I'm at " + next);
                    System.out.println("moved: " + moved);
                    if(moved) {
                        stack.push(next);
                        push = true;
                        break;  // continue won't work here because it's inside the for-loop before the while loop
                    }
                }
            }

            if(push)
                continue;

            stack.pop();
            State prev = !stack.isEmpty() ? stack.peek() : null;
            if(prev != null) {
                int rowD = prev.r - curr.r;
                int colD = prev.c - curr.c;
                int reverseDir;
                if (rowD == ROW[0] && colD == COL[0]) //up
                    reverseDir = 0;
                else if (rowD == ROW[1] && colD == COL[1]) //right
                    reverseDir = 1;
                else if (rowD == ROW[2] && colD == COL[2]) //down
                    reverseDir = 2;
                else // left
                    reverseDir = 3;

                System.out.println("PREV DIR: " + rowD + "," + colD);

                while (dir != reverseDir) {
                    dir = (dir + 1) % 4;
                    robot.turnRight(1);
                }

                robot.move();

                System.out.println("robot direction: " + robot.direction() + ", cached direction: " + getDirection(dir));
                System.out.println("robot position: " + robot.position() + ", cached position: " + prev);
            }
        }

        robot.printCleanMap();
    }

    private static boolean outOfBounds(int[][] M, int r, int c) {
        return r < 0 || r >= M.length || c < 0 || c >= M[0].length || M[r][c] == 1;
    }

    private static String getDirection(int dir) {
        switch(dir) {
            case 0 : return "up";
            case 1 : return "left";
            case 2 : return "down";
            default: return "right!";
        }
    }

    static class State {
        int r;
        int c;

        public State(int r, int c) {
            this.r = r;
            this.c = c;
        }

        @Override
        public boolean equals(Object o) {
            State other = o != null ? (State)o : null;
            return other != null && this.r == other.r && this.c == other.c;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + r;
            result = prime * result + c;
            return result;
        }

        public String toString() {
            return "(" + r + "," + c + ")";
        }
    }

    private static class Robot {
        int[][] M;
        int r;
        int c;
        int direction;
        int[][] cleaned;

        public Robot(int[][] M, int r, int c) {
            this.M = M;
            this.r = r;
            this.c = c;
            this.direction = 0;

            this.cleaned = new int[M.length][M[0].length];

            System.out.println("Robot at " + r + ", " + c + " facing " + direction());
        }

        public void clean() {
            if (this.cleaned[r][c] == 0) {
                this.cleaned[r][c] = 1;
            }
            else {
                System.out.println("Already cleaned " + r + ", " + c);
            }
        }

        public boolean move() {
            if (direction == 0 && !outOfBounds(M,r - 1, c)) {
                r -= 1;
                System.out.println("Moved to " + r + ", " + c);
                return true;
            }
            else if (direction == 1 && !outOfBounds(M, r, c + 1)) {
                c += 1;
                System.out.println("Moved to " + r + ", " + c);
                return true;
            }
            else if (direction == 2 && !outOfBounds(M, r + 1, c)) {
                r += 1;
                System.out.println("Moved to " + r + ", " + c);
                return true;
            }
            else if (direction == 3 && !outOfBounds(M, r, c - 1)) {
                c -= 1;
                System.out.println("Moved to " + r + ", " + c);
                return true;
            }

            System.out.println("Could not move to " + (r + ROW[direction]) + ", " + (c + COL[direction]) );
            return false;
        }

        public void turnLeft(int n) {
            if (n < 0) return;
            direction = (direction - n) % 4;
            direction += direction < 0 ? 4 : 0;
            System.out.println("Turned " + direction());
        }

        public void turnRight(int n) {
            if (n < 0) return;
            direction = (direction + n) % 4;
            System.out.println("Turned " + direction());
        }

        private String direction() {
            switch(direction) {
                case 0 : return "up";
                case 1 : return "left";
                case 2 : return "down";
                default: return "right";
            }
        }

        private String position() {
            return "(" + r + ", " + c + ")";
        }

        private void printCleanMap() {
            for(int[] c : cleaned) {
                for(int i : c)
                    System.out.print(i);
                System.out.println();
            }
        }
    }
}
