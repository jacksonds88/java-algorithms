package google;

import java.util.LinkedList;

public class LovelyLuckyLambsT2 {
    public static void main(String[] args) {
        //System.out.println(stingy(15));

        //System.out.println(stingy(27));
        System.out.println(generous(30));
        //System.out.println(solution(10));

        //System.out.println(solution(143));
    }

    static int solution(int L) {
        return stingy(L) - generous(L);
    }

    static int stingy(int L) {
        if(L <= 1)
            return L;

        LinkedList<Integer> list = new LinkedList<>();
        int H = 2;
        int a = 1;
        int b = 1;
        list.add(a);
        list.add(b);
        int f = b + a;
        L -= f;
        while(L > 0) {
            f = b + a;

            a = b;
            b = f;
            System.out.println(f);
            if(f <= L) {
                H++;
                list.add(f);
            }
            L -= f;
        }

        int sum = 0;
        for(int x : list) {
            System.out.print(x + ",");
            sum += x;
        }
        System.out.println();
        System.out.println("sum: " + sum);
        System.out.println("count: " + list.size());
        System.out.println("H: " + H);
        System.out.println();
        return H;
    }

    static int generous(int L) {
        if(L <= 2)
            return L;

        LinkedList<Integer> list = new LinkedList<>();


        int H = 2;
        int[] LP = new int[2];
        LP[0] = 1;
        LP[1] = 2;
        list.add(LP[0]);
        list.add(LP[1]);
        L -= LP[0] + LP[1];

        // 1, 2, 4, 8
        while(L > 0) {
            int max = LP[1] * 2;
            int diff = L < max ? L : max;
            L -= diff;
            if(diff < LP[0] + LP[1])
                break;
            else {
                H++;
                list.add(diff);
                LP[0] = LP[1];
                LP[1] = diff;
            }
        }

        int sum = 0;
        for(int x : list) {
            System.out.print(x + ",");
            sum += x;
        }
        System.out.println();
        System.out.println("sum: " + sum);
        System.out.println("count: " + list.size());
        System.out.println("H: " + H);
        System.out.println();

        return H;
    }
}
