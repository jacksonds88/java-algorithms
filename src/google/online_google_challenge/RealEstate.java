package google;

import java.util.LinkedList;
import java.util.Queue;

/*
    Source: https://www.careercup.com/question?id=4812770682863616

    Solution incomplete.
    Must solve in rectangles!!!

 */

public class RealEstate {
    public static void main(String[] args) {
        int[][] M = new int[3][3];
        M[0][0] = 4;
        M[0][1] = 6;
        M[0][2] = 7;

        M[1][0] = 3;
        M[1][1] = 5;
        M[1][2] = 2;

        M[2][0] = 2;
        M[2][1] = 4;
        M[2][2] = 5;

        largest2(M, 16, 2, 0);
    }

    static int largest(int[][] M, int B, int i, int j) {
        Queue<String> Q = new LinkedList<>();
        int[][] C = new int[M.length][M[0].length];
        boolean[][] V = new boolean[M.length][M[0].length];
        Q.add(i + "" + j);
        C[i][j] = M[i][j];


        while(!Q.isEmpty()) {
            String p = Q.poll();
            System.out.println(p);
            i = Character.getNumericValue(p.charAt(0));
            j = Character.getNumericValue(p.charAt(1));
            V[i][j] = true;

            if(!outOfBounds(M, i+1, j) && !V[i+1][j] && B >= C[i][j] + M[i+1][j]) {
                String next = (i+1) + "" + j;
                System.out.println("next: " + next);
                if(!Q.contains(next))
                    Q.add(next);
                if(C[i+1][j] < C[i][j] + M[i+1][j])
                    C[i+1][j] = C[i][j] + M[i+1][j];
            }

            if(!outOfBounds(M, i, j+1) && !V[i][j+1] && B >= C[i][j] + M[i][j+1]) {
                String next = i + "" + (j+1);
                System.out.println("next: " + next);

                if(!Q.contains(next))
                    Q.add(next);
                if(C[i][j+1] < C[i][j] + M[i][j+1])
                    C[i][j+1] = C[i][j] + M[i][j+1];
            }

            if(!outOfBounds(M, i-1, j) && !V[i-1][j] && B >= C[i][j] + M[i-1][j]) {
                String next = (i-1) + "" + j;
                System.out.println("next: " + next);

                if(!Q.contains(next))
                    Q.add(next);
                if(C[i-1][j] < C[i][j] + M[i-1][j])
                    C[i-1][j] = C[i][j] + M[i-1][j];
            }

            if(!outOfBounds(M, i, j-1) && !V[i][j-1] && B >= C[i][j] + M[i][j-1]) {
                String next = i + "" + (j-1);
                System.out.println("next: " + next);

                if(!Q.contains(next))
                    Q.add(next);
                if(C[i][j-1] < C[i][j] + M[i][j-1])
                    C[i][j-1] = C[i][j] + M[i][j-1];
            }
        }

        System.out.println();
        for(int[] c : C) {
            for(int x : c) {
                System.out.print(x + ", ");
            }
            System.out.println();
        }

        System.out.println();
        for(boolean[] c : V) {
            for(boolean b : c) {
                System.out.print(b + ", ");
            }
            System.out.println();
        }

        return 0;
    }

    // you can only purchase in rectangles
    static int largest2(int[][] M, int B, int i, int j) {
        Queue<String> Q = new LinkedList<>();
        int[][] C = new int[M.length][M[0].length];
        boolean[][] V = new boolean[M.length][M[0].length];
        Q.add(i + "" + j);
        C[i][j] = M[i][j];


        while(!Q.isEmpty()) {
            String p = Q.poll();
            System.out.println(p);
            i = Character.getNumericValue(p.charAt(0));
            j = Character.getNumericValue(p.charAt(1));
            V[i][j] = true;

            if(!outOfBounds(M, i+1, j) && !V[i+1][j]) {
                String next = (i+1) + "" + j;
                System.out.println("next: " + next);
                if(!Q.contains(next))
                    Q.add(next);
                C[i+1][j] = C[i][j] + M[i+1][j];
            }

            if(!outOfBounds(M, i, j+1) && !V[i][j+1]) {
                String next = i + "" + (j+1);
                System.out.println("next: " + next);

                if(!Q.contains(next))
                    Q.add(next);
                C[i][j+1] = C[i][j] + M[i][j+1];
            }

            if(!outOfBounds(M, i-1, j) && !V[i-1][j]) {
                String next = (i-1) + "" + j;
                System.out.println("next: " + next);

                if(!Q.contains(next))
                    Q.add(next);
                C[i-1][j] = C[i][j] + M[i-1][j];
            }

            if(!outOfBounds(M, i, j-1) && !V[i][j-1]) {
                String next = i + "" + (j-1);
                System.out.println("next: " + next);

                if(!Q.contains(next))
                    Q.add(next);
                C[i][j-1] = C[i][j] + M[i][j-1];
            }
        }

        System.out.println();
        for(int[] c : C) {
            for(int x : c) {
                System.out.print(x + ", ");
            }
            System.out.println();
        }

        System.out.println();
        for(boolean[] c : V) {
            for(boolean b : c) {
                System.out.print(b + ", ");
            }
            System.out.println();
        }

        return 0;
    }

    static boolean outOfBounds(int[][] M, int i, int j) {
        return (i < 0 || i >= M.length) || (j < 0 || j >= M[0].length);
    }
}
