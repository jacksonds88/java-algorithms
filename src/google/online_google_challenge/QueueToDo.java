package google;

public class QueueToDo {
    public static void main(String[] args) {
        System.out.println(solution(0, 3));
        System.out.println(solution(17, 4));
        System.out.println(solution(0, 1));
        System.out.println(solution(33, 1));

    }

    static int solution2(int s, int l) {
        int count = l;
        int xor = 0;
        for(int i = 0; i < l - 1; i++) {
            int sum;
            if(count > 1) {
                double avg = (s + (s + count - 1)) / 2d;
                System.out.println(s + " + " + (s + count - 1));
                System.out.println("avg: " + avg);
                sum = (int) (avg * l);
                System.out.println(sum);
            } else
                sum = s;
            xor ^= sum;
            s = s + l;
            System.out.println(s);
            System.out.println();
            count--;
        }

        return xor;
    }

    static int solution(int start, int length) {
        int count = length;
        int xor = 0;
        for(int i = 0; i < length; i++) {
            for(int j = start; j < (start+count); j++)
                xor ^= j;
            start += length;
            count--;
        }

        return xor;
    }

}
