package google;

/*
    Determine whether a circular array of relative indices is composed of a single complete cycle
 */

import java.util.HashSet;

public class CircularArray {
    public static void main(String[] args) {
        int[] A1 = {3, 1, 0, -2};  // false
        int[] A2 = {2, 2, -1};  // true, contains a complete cycle
        int[] A3 = {1, 1, -1};  // false
        int[] A4 = {2, -1};  // false
        int[] A5 = {-1, -1};  // true
        int[] A6 = {1, 1, -2, 1, 1, 1};  // true

        System.out.println(solution(A1));  // false
        System.out.println(solution(A2));  // true
        System.out.println(solution(A3));  // false
        System.out.println(solution(A4));  // false
        System.out.println(solution(A5));  // true
        System.out.println(solution(A6));  // false
        System.out.println();

        System.out.println(solution2(A1));  // false
        System.out.println(solution2(A2));  // true
        System.out.println(solution2(A3));  // false
        System.out.println(solution2(A4));  // false
        System.out.println(solution2(A5));  // true
        System.out.println(solution2(A6));  // false
        System.out.println();

        System.out.println(solution3(A1));  // false
        System.out.println(solution3(A2));  // true
        System.out.println(solution3(A3));  // false
        System.out.println(solution3(A4));  // false
        System.out.println(solution3(A5));  // true
        System.out.println(solution3(A6));  // true
        System.out.println();

        System.out.println(solution4(A1));  // false
        System.out.println(solution4(A2));  // true
        System.out.println(solution4(A3));  // false
        System.out.println(solution4(A4));  // false
        System.out.println(solution4(A5));  // true
        System.out.println(solution4(A6));  // false

    }

    // solved in-place
    // time complexity is linear (n)
    static boolean solution(int[] A) {
        if(A == null || A.length == 0)
            return false;

        int previousPos = -1;
        int currentPos = 0;
        int sum = 0;
        for(int i = 0; i < A.length; i++) {
            if(A[currentPos] == 0)
                return false;

            currentPos = (currentPos + A[currentPos]) % A.length;
            if(currentPos < 0)
                currentPos += A.length;
            sum += A[currentPos];

            if(currentPos == previousPos)
                return false;
            previousPos = currentPos;
        }

        return currentPos == 0 && Math.abs(sum) == A.length; // where it started
    }

    static boolean solution2(int[] A) {
        if(A == null || A.length == 0)
            return false;

        boolean[] B = new boolean[A.length];
        int currentPos = 0;
        for(int i = 0; i < A.length; i++) {
            currentPos = (currentPos + A[currentPos]) % A.length;
            if(currentPos < 0)
                currentPos = A.length + currentPos;
            B[currentPos] = true;
        }

        for(boolean b : B)
            if(!b)
                return false;
        return true;
    }

    static boolean solution3(int[] A) {
        if(A == null || A.length == 0)
            return false;

        HashSet<Integer> B = new HashSet<>();
        int currentPos = 0;
        for(int i = 0; i < A.length; i++) {
            currentPos = (currentPos + A[currentPos]) % A.length;
            if(currentPos < 0)
                currentPos = A.length + currentPos;
            B.add(currentPos);
        }

        if(B.size() != A.length)
            return false;

        for(int b : B)
            if(b < 0 || b >= A.length)
                return false;

        return true;
    }

    // linear space, but alters the array
    static boolean solution4(int[] A) {
        if(A == null || A.length == 0)
            return false;

        int previousPos;
        int currentPos = 0;
        for(int i = 0; i <= A.length; i++) {
            if(i == A.length)
                return currentPos == 0;

            if(A[currentPos] == 0)  // zeros cycle infinitely, also used to track previous iterations
                return false;

            previousPos = currentPos;
            currentPos = (currentPos + A[currentPos]) % A.length;
            if(currentPos < 0)
                currentPos += A.length;
            A[previousPos] = 0;
        }

        return true;
    }
}
