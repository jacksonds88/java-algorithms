package google;

import java.util.LinkedList;
import java.util.Queue;

public class MarkovChain {
    public static void main(String[] args) {
        int[][] m = new int[5][5];
        m[0] = new int[]{0, 2, 1, 0, 0};
        m[1] = new int[]{0, 0, 0, 3, 4};
        m[2] = new int[]{0, 0, 0, 0, 0};
        m[3] = new int[]{0, 0, 0, 0, 0};
        m[4] = new int[]{0, 0, 0, 0, 0};

        for(int[] n : m) {
            for (int x : n)
                System.out.print(x);
            System.out.println();
        }


        int[] s = solution(m);
        for (int x : s)
            System.out.print(x + ", ");
        System.out.println();
    }

    static int[] solution(int[][] A) {
        State[][] S = generateS(A);
        Queue<State> Q = new LinkedList<>();
        LinkedList<State> states = new LinkedList<>();
        int maxD = 0;

        for(State s : S[0])
            if(s != null) {
                Q.add(s);
            }

        while(!Q.isEmpty()) {
            State curr = Q.poll();
            states.add(curr);
            boolean added = false;
            for(int i = 0; i < A.length; i++) {
                State r = S[curr.r][i];
                if(r != null) {
                    r.n *= curr.n;
                    r.d *= curr.d;
                    if(r.d > maxD)
                        maxD = r.d;
                    states.add(r);
                    added = true;
                }
            }

            if(added)  // current is an intermediate path
                states.remove(curr);
        }

        normalize(states, maxD);

        System.out.println();
        for(State[] M : S) {
            for (State s : M)
                if(s != null)
                    System.out.print(s + ", ");
            System.out.println();
        }
        System.out.println();

        int[] ret = new int[states.size() + 1];
        int i = 0;
        for(State s : states)
            ret[i++] = s.n;
        ret[i] = maxD;
        return ret;
    }

    static State[][] generateS(int[][] A) {
        int[] D = new int[A.length]; // denominator of each row
        State[][] S = new State[A.length][A.length];
        Queue<Integer> Q = new LinkedList<>();
        Q.add(0);

        while(!Q.isEmpty()) {
            int curr = Q.poll();
            System.out.println("curr: " + curr);
            int sum = 0;
            for(int i = 0; i < A.length; i++) {
                int r = A[curr][i];
                sum += r;
                if(r > 0) {
                    System.out.println("Q: " + i);
                    Q.add(i);
                    S[curr][i] = new State(curr, i, r);
                }
            }

            D[curr] = sum;
            for(State s : S[curr])
                if(s != null)
                    s.d = sum;
            System.out.println(sum);
        }

        return S;
    }

    static void normalize(LinkedList<State> states, int maxDenominator) {
        for(State s : states) {
            if(s.d < maxDenominator) {
                if(divides(s.d, maxDenominator)){
                    int d = maxDenominator / s.d;
                    s.n *= d;
                    s.d *= d;
                }
            }
            System.out.println(s);
        }
    }

    static boolean divides(int a, int b) {
        return b % a == 0;
    }

    static int gcd(int a, int b) {
        while(b != 0) {
            int t = b;
            b = a % b;
            a = t;
        }

        return a;
    }

    static class State {
        int s; // state
        int r; // next state
        int n; // numerator
        int d; // denominator

        public State(int s, int r, int n) {
           this.s = s;
           this.r = r;
           this.n = n;
        }

        public String toString() {
            return s + " to " + r + ": " + n + "/" + d;
        }
    }
}
