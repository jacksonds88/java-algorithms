package google;

import java.util.Iterator;
import java.util.LinkedList;

/*
    Level 1 of the Google challenge. See resources for the instructions.
 */

public class SolarPanelT1 {
    static int count = 0;
    public static void main(String[] args) {
        //int[] A = solution(12);
        int[] A = solution(15324);
        System.out.println(count);

        for(int x : A)
            System.out.print(x + ", ");
        System.out.println();
    }

    static int[] solution(int x) {
        LinkedList<Integer> L = new LinkedList<>();
        while(x > 0) {
            // can be done without the if-else statement, but sqrt is an expensive operation so try to avoid
            if(x > 2) {
                double y = (int) Math.sqrt(x);
                int z = (int) y;
                L.add(z*z);
                x = x - (z * z);
            } else {
                x--;
                L.add(1);
            }
        }

        int[] A = new int[L.size()];
        Iterator<Integer> it = L.iterator();
        for(int i = 0; i < A.length; i++)
            A[i] = it.next();

        return A;
    }
}
