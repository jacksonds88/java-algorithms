package google;

import java.util.LinkedList;
import java.util.Stack;

public class MagicNumbers {
    public static void main(String[] args) {
        // 10, [1,2]
        // 2, [1,2,3,4]
        System.out.println(solution2(10, new int[]{1, 2}));
        System.out.println(solution2(2, new int[]{1, 2, 3, 4}));
        //System.out.println(solutionIteration(10, new int[]{1, 2}));
        //System.out.println(solutionIteration(2, new int[]{1, 2, 3, 4}));
    }

    static boolean solution(int target, int[] A) {
        if(A == null || A.length == 0)
            return false;

        LinkedList<Integer> history = new LinkedList<>();
        solutionRecursion(target, A, 1, A[0], history);
        return !history.isEmpty();
    }

    static void solutionRecursion(int target, int[] A, int i, int carry, LinkedList<Integer> history) {
        if(i < A.length) {
            int sum = carry + A[i];
            solutionRecursion(target, A, i+1, sum, history);

            int difference = carry - A[i];
            solutionRecursion(target, A, i + 1, difference, history);
        }

        System.out.println(carry + " == " + target);
        if(carry == target)
            history.add(carry);
    }

    static boolean solution2(int target, int[] A) {
        if(A == null || A.length == 0)
            return false;
        return solutionRecursion2(target, A, 1, A[0]);
    }

    static boolean solutionRecursion2(int target, int[] A, int i, int carry) {
        if(i == A.length)
            return target == carry;

        return solutionRecursion2(target, A, i+1, carry + A[i]) || solutionRecursion2(target, A, i + 1, carry - A[i]);
    }

    static boolean solutionIteration(int target, int[] A) {
        if(A == null || A.length == 0)
            return false;

        Stack<Parameters> stack = new Stack<>();
        stack.add(new Parameters(A[0], 1));
        while(!stack.isEmpty()) {

            Parameters curr = stack.pop();
            if(curr.i < A.length) {
                Parameters add = new Parameters(curr.carry + A[curr.i], curr.i + 1);
                Parameters subtract = new Parameters(curr.carry - A[curr.i], curr.i + 1);

                stack.add(add);
                stack.add(subtract);
            } else {
                if (curr.carry == target)
                    return true;
            }

        }

        return false;
    }

    static class Parameters {
        int carry;
        int i;

        public Parameters(int carry, int i) {
            this.carry = carry;
            this.i = i;
        }
    }
}
