package facebook;

/*
    Write a method that searches an array of Strings (e.g., [abc, biz, bust, ...]) for some input String. One caveat is
    the character "." is a wildcard, so it can be any single character (that does not include the empty character).
    Assume the array of Strings is a global variable, and the solution method is named "boolean isMember(String s)"

    For the 2nd part, how would you optimize the searching that is not O(n) where n is the size of the String array.
    Use a binary tree to accomplish this. The leaves of the tree should be the words, and the parent nodes should be
    substring paths to the leaf (word).
 */

import java.util.Stack;

public class Dictionary {

    static String[] dictionary = new String[]{"abc", "biz", "bust"};
    static final char WILDCARD = '.';

    public static void main(String[] args) {
        System.out.println(isMember("abc"));
        System.out.println(isMember("xyz"));
        System.out.println(isMember("xyz"));

        System.out.println("aaab".compareTo("aaac"));

        BinaryTree bt = new BinaryTree();
        bt.insert("abc");
        bt.insert("biz");
        bt.insert("bust");

        System.out.println("Assert True: " + bt.search("abc"));
        System.out.println("Assert False: " + bt.search("xyz"));
        System.out.println("Assert True: " + bt.search("biz"));
    }

    static boolean isMember(String s){

        for(String word : dictionary) {
            if(word.length() != s.length())
                continue;

            boolean equals = true;
            for (int i = 0; i < s.length(); i++)
                if(s.charAt(i) != WILDCARD && s.charAt(i) != word.charAt(i)) {
                    equals = false;
                    break;
                }

            if(equals)
                return true;
        }

        return false;
    }

    static class BinaryTree {

        static class Node {
            String data;
            Node left;
            Node right;
            public Node(String data) {
                this.data = data;
            }

            public String toString() {
                return data;
            }
        }

        Node root = null;

        public BinaryTree() {

        }

        public void insert(String data) {
            for(int i = 0; i <= data.length(); i++)
                insert2(data.substring(0, i));
        }

        private void insert2(String data) {
            if(root == null) {
                root = new Node(data);
            } else {
                Stack<Node> stack = new Stack<>();
                stack.push(root);

                while(true) {

                    if(data.compareTo(stack.peek().data) == 0) {
                        System.out.println("already exists!");
                        return;
                    } else if(data.compareTo(stack.peek().data) < 0) {  // smaller
                        if(stack.peek().left == null) {
                            stack.peek().left = new Node(data);
                            System.out.println("Added: " + stack.peek().left);
                            return;
                        }

                        stack.push(stack.peek().left);
                    } else //if(data.compareTo(stack.peek().data) > 0) {  // bigger
                        if(stack.peek().right == null) {
                            stack.peek().right = new Node(data);
                            System.out.println("Added: " + stack.peek().right);
                            return;
                        }
                        stack.push(stack.peek().right);
                    }
                }
            }

            public boolean search(String data) {
                Stack<Node> stack = new Stack<>();
                stack.push(root);

                while(stack.peek() != null) {
                    if(data.compareTo(stack.peek().data) == 0)
                        return true;
                    else if(data.compareTo(stack.peek().data) < 0) { // smaller
                        stack.push(stack.peek().left);
                    } else
                        stack.push(stack.peek().right);
                }

                return false;
            }


    }
}
