package facebook;

import java.util.HashSet;

/*
    Powerset
    Source: https://youtu.be/bGC2fNALbNU
 */

public class PowerSet {
    public static void main(String[] args) {
        int[] A = {1, 2};
        output(powerSet(A));
    }

    static void output(HashSet<HashSet<Integer>> H) {
        for(HashSet<Integer> h : H) {
                System.out.println();
            for(Integer x : h)
                if(x == null)
                    System.out.print("_");
                else
                    System.out.print(x + ",");
        }
    }

    static HashSet<HashSet<Integer>> powerSet(int[] A) {
        HashSet<HashSet<Integer>> H = new HashSet<>();
        HashSet<Integer> S = new HashSet<>();
        for(int a : A)
            S.add(a);
        HashSet<Integer> R = new HashSet<>();
        R.add(null);
        H.add(R); // add empty set
        H.add(S); // add full set
        return powerSet(S, H);
    }

    static HashSet<HashSet<Integer>> powerSet(HashSet<Integer> S, HashSet<HashSet<Integer>> H) {
        if(S == null || S.size() == 0) {
            return H;
        } else {
            for(int x : S) {
                if(S.size() > 1) {
                    HashSet<Integer> R = new HashSet<>(S);
                    R.remove(x);
                    H.add(R);
                    powerSet(R, H);
                }
            }
        }

        return H;
    }
}
