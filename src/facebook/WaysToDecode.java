package facebook;

// Source: https://www.youtube.com/watch?v=qli-JCrSwuk&t=116s

public class WaysToDecode {
    public static void main(String[] args) {
        //System.out.println(decode("12"));  // 2
        //System.out.println(decode("3"));  // 1
        //System.out.println(decode("345"));  // 1
        //System.out.println(decode("27345"));  // 1
        System.out.println(decodeDp("27345"));  // 1
        //System.out.println(decodeDp("1212"));  // 5
        //System.out.println(decodeDp("1111"));  // 5

        //System.out.println(decode("27"));  // 1
        //System.out.println(contains("27"));
    }

    // O(2^n) without dynamic programming, because recursion behaves like a binary tree in paths
    static int decode(String data) {
        return decodeHelper(data, data.length());
    }

    static int decodeHelper(String data, int k) {
        System.out.println("decodeHelper");

        if(k == 0)
            return 1;

        int s = data.length() - k;
        if(data.charAt(s) == '0')
            return 0;

        int result = decodeHelper(data, k-1);
        if(k >= 2 && contains(data.charAt(k-1) + "" + data.charAt(k-2)))
            result += decodeHelper(data, k-2);
        return result;
    }

    // linear, O(n)
    static int decodeDp(String data) {
        int[] memo = new int [data.length() + 1];
        return decodeHelperDp(data, data.length(), memo);
    }

    static int decodeHelperDp(String data, int k, int[] memo) {
        System.out.println("decodeHelperDp");
        if(k == 0)
            return 1;

        int s = data.length() - k;
        if(data.charAt(s) == '0')
            return 0;

        if(memo[k] != 0)
            return memo[k];

        int result = decodeHelperDp(data, k-1, memo);
        if(k >= 2 && contains(data.charAt(k-1) + "" + data.charAt(k-2)))
            result += decodeHelperDp(data, k-2, memo);
        memo[k] = result;
        return result;
    }

    static int decode2(String data) {
        return decodeHelper(data, 0);
    }

    // does not work
    static int decodeHelper2(String data, int k) {
        if(k >= data.length())
            return 1;

        //int s = data.length() - k;
        if(data.charAt(k) == '0')
            return 0;

        int result = decodeHelper(data, k+1);
        if(k < data.length() - 2 && contains(data.charAt(k+1) + "" + data.charAt(k+2)))
            result += decodeHelper(data, k+2);
        return result;
    }

    // 97 to 122
    static boolean contains(String s) {
        int i = Integer.parseInt(s);
        return i > 0 && i < 27;
    }
}
