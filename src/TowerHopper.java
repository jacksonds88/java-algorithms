// Source: https://www.youtube.com/watch?v=kHWy5nEfRIQ

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class TowerHopper {
    public static void main(String[] args) {
        int[] heights = {4, 2, 0, 0, 2, 0};
        int[] heights2 = {2, 2, 0, 0, 2, 0};
        int[] heights3 = {1, 3, 5, 3, 1, 0};

        System.out.println(isHoppable(heights));
        System.out.println(isHoppable(heights2));

        System.out.println(leastHops(heights3));

    }

    static boolean isHoppable(int[] heights) {
        int[][] G = new int[heights.length][heights.length];
        for(int i = 0; i < G.length; i++) {
            System.out.println(heights[i]);
            for (int j = i + 1; j < G[i].length && j <= (i + heights[i]); j++) {
                G[i][j] = 1;
            }
        }

        for(int[] g : G)
            System.out.println(Arrays.toString(g));

        Queue<Integer> Q = new LinkedList<>();
        Q.add(0);

        while(!Q.isEmpty()) {
            int curr = Q.poll();
            System.out.println("CURR: " + curr);
            if(heights[curr] + curr >= heights.length)
                return true;

            for(int i = curr + 1; i < G.length; i++) {
                if (G[curr][i] > 0
                        && heights[i] > 0
                        && heights[curr] - (i - curr) >= 0) {
                    Q.add(i);
                }
            }
        }

        return false;
    }

    static int leastHops(int[] heights) {
        int[][] G = new int[heights.length][heights.length];
        for(int i = 0; i < G.length; i++) {
            System.out.println(heights[i]);
            for (int j = i + 1; j < G[i].length && j <= (i + heights[i]); j++) {
                G[i][j] = 1;
            }
        }

        for(int[] g : G)
            System.out.println(Arrays.toString(g));

        Queue<Integer> Q = new LinkedList<>();
        int[] hops = new int[heights.length];
        for(int i = 0; i < hops.length; i++)
            hops[i] = Integer.MAX_VALUE;
        Q.add(0);
        hops[0] = 0;

        while(!Q.isEmpty()) {
            int curr = Q.poll();
            System.out.println("CURR: " + curr);
            if(heights[curr] + curr >= heights.length) {
                System.out.println(Arrays.toString(hops));
                return hops[curr] + 1;
            }

            for(int i = curr + 1; i < G.length; i++) {
                if (G[curr][i] > 0
                        && heights[i] > 0
                        && heights[curr] - (i - curr) >= 0) {
                    Q.add(i);
                    if(hops[i] == Integer.MAX_VALUE || hops[i] + 1 < hops[curr])
                        hops[i] = hops[curr] + 1;
                }
            }
        }

        return -1;
    }
}
