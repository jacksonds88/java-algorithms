import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;

// PowerSet algorithm produces 2^n subsets, so n = 5 will produce 2^5 = 32 subsets

public class PowerSet {

    static int count = 0;

    public static void main(String[] args) {

        HashSet<String> S = new HashSet<>();
        S.add("a");
        S.add("b");
        S.add("c");
        S.add("d");
        HashSet<HashSet<String>> powerSet = PS(S, new HashSet<>());
        System.out.println("PS had " + count + " recursive calls");  // 65
        count = 0;

        for(HashSet<String> subset : powerSet) {
            for (String s : subset)
                System.out.print(s);
            System.out.println();
        }

        powerSet = PSWithMemoization(S, new HashSet<>());
        System.out.println("PSWithMemoization had " + count + " recursive calls");  // 16
        count = 0;

        for(HashSet<String> subset : powerSet) {
            for (String s : subset)
                System.out.print(s);
            System.out.println();
        }
    }

    static HashSet<HashSet<String>> PS(HashSet<String> T, HashSet<HashSet<String>> S) {
        count++;
        S.add(T);

        if(T.isEmpty())
            return S;

        for(String s : T) {
            HashSet<String> t = new HashSet<>(T);
            t.remove(s);
            PS(t, S);
        }

        return S;
    }

    static HashSet<HashSet<String>> PSWithMemoization(HashSet<String> T, HashSet<HashSet<String>> S) {
        count++;
        S.add(T);

        if(T.isEmpty())
            return S;

        for(String s : T) {
            HashSet<String> t = new HashSet<>(T);
            t.remove(s);

            if(!S.contains(t))
                PSWithMemoization(t, S);
        }

        return S;
    }

    // iterative top-down solution
    static HashSet<HashSet<String>> PSWithIteration(HashSet<String> fullSet) {
        Queue<HashSet<String>> queue = new LinkedList<>();
        queue.add(fullSet);
        HashSet<HashSet<String>> masterSet = new HashSet<>();
        masterSet.add(fullSet);

        while(!queue.isEmpty()) {
            HashSet<String> current = queue.poll();
            for (String s : current) {
                HashSet<String> newSet = new HashSet<>(current);
                newSet.remove(s);

                if(!masterSet.contains(newSet)) {
                    queue.add(newSet);
                    masterSet.add(newSet);
                }
            }
        }

        return masterSet;
    }
}
