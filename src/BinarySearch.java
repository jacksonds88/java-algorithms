public class BinarySearch {
    public static void main(String[] args) {
        int[] A = new int[17];
        A[0] = 1;
        A[1] = 3;
        A[2] = 4;
        A[3] = 6;
        A[4] = 7;
        A[5] = 8;
        A[6] = 10;
        A[7] = 13;
        A[8] = 14;
        A[9] = 18;
        A[10] = 19;
        A[11] = 21;
        A[12] = 24;
        A[13] = 37;
        A[14] = 40;
        A[15] = 45;
        A[16] = 71;

        System.out.println(exists(7, A));
        System.out.println(exists(73, A));

    }

    static boolean exists(int x, int[] A) {
        return exists(x, A, 0, A.length);
    }

    static boolean exists(int x, int[] A, int lower, int upper) {

        int shift = (upper - lower) / 2;
        int middle = shift + lower;

        if(x == A[middle])
            return true;
        else if(shift == 0)
            return false;
        else if(x > A[middle])
            return exists(x, A, middle, upper);
        else  // x < A[middle]
            return exists(x, A, lower, middle);
    }
}
