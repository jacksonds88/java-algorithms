import java.util.HashSet;
import java.util.LinkedList;
import java.util.Stack;

public class DFS {
    static String[] N = new String[]{"A", "B", "C", "D", "E", "F"};

    public static void main(String[] args) {
        int[][] M = new int[6][6];
        M[0][1] = 7;
        M[0][2] = 9;
        M[0][5] = 14;

        M[1][0] = 7;
        M[1][2] = 10;
        M[1][3] = 15;

        M[2][0] = 9;
        M[2][1] = 10;
        M[2][3] = 11;
        M[2][5] = 2;

        M[3][1] = 15;
        M[3][1] = 11;
        M[3][3] = 6;

        M[4][3] = 6;
        M[4][5] = 9;

        M[5][0] = 14;
        M[5][2] = 2;
        M[5][4] = 9;

        LinkedList<String> T = solution(N, M);  // should give DEFCBA
        for(String s : T)
            System.out.print(s);
        System.out.println();

        solution2(M);
        System.out.println();

        solution3(M);
    }

    static LinkedList<String> solution(String[] N, int[][] M) {
        LinkedList<String> dfs = new LinkedList<>();
        Stack<Integer> S = new Stack<>();
        HashSet<Integer> V = new HashSet<>();
        S.push(0);
        V.add(0);

        while(!S.isEmpty()) {
            int s = S.peek();
            V.add(s);
            boolean pushed = false;

            for(int i = 0; i < M[s].length; i++) {
                if(M[s][i] > 0 && !V.contains(i) && !S.contains(i)) {
                    S.push(i);
                    pushed = true;
                    break;
                }
            }

            if(pushed)
                continue;

            s = S.pop();
            dfs.add(N[s]);
        }

        return dfs;
    }

    // iterative psuedo code: https://en.wikipedia.org/wiki/Depth-first_search
    static void solution3(int[][] M) {
        Stack<Integer> S = new Stack<>();
        HashSet<Integer> V = new HashSet<>();
        S.push(0);
        V.add(0);

        while(!S.isEmpty()) {
            int s = S.pop();
            System.out.println(s);
            V.add(s);

            for(int i = 0; i < M[s].length; i++) {
                if(M[s][i] > 0 && !V.contains(i) && !S.contains(i)) {
                    S.push(i);
                }
            }
        }
    }

    static void solution2(int[][] M) {
        solution2(M, 0, new HashSet<>());
    }

    static void solution2(int[][] M, int s, HashSet<Integer> V) {
        V.add(s);

        for(int i = 0; i < M[s].length; i++) {
            if(M[s][i] > 0 && !V.contains(i)) {
                solution2(M, i, V);
            }
        }

        System.out.println(N[s]);
    }
}
