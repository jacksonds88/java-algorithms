public class MaxHeap {
    public static void main(String[] args) {
        int[] A = {9, 6, 8, 2, 5, 7};

        Heap maxHeap = new Heap(A.length);
        for(int a : A)
            maxHeap.insert(a);

        for(int x : maxHeap.H)
            System.out.print(x + ", ");
        System.out.println();
        System.out.println();

        for(int i = 0; i < A.length; i++)
           System.out.println(maxHeap.pop());
    }

    static class Heap {
        int[] H;
        int size;

        public Heap(int size) {
            H = new int[size];
        }

        public void insert(int x) {
            // if size == H.length then double()
            H[size] = x;
            size++;

            for(int i = size; i > 1; i = i /2) {
                int p = (i - 1) / 2;
                if(H[i-1] > H[p])
                    swap(i-1, p);
                else
                    break;
            }
        }

        public int pop() {
            if(size <= 0)
                return 0;
            size--;
            int ret = H[0];
            int i = 0;
            swap(i, size);
            H[size] = 0;


            int l = 2*i + 1;
            int r = 2*i + 2;

            while(i < size - 1) {
                int smaller = i;
                if(l < size && H[smaller] < H[l])
                    smaller = l;
                if(r < size && H[smaller] < H[r])
                    smaller = r;

                if(H[i] == H[smaller])
                    break;
                else
                    swap(i, smaller);

                i = smaller;
                l = 2*i + 1;
                r = 2*i + 2;
            }

            return ret;
        }

        void swap(int i, int j) {
            int temp = H[i];
            H[i] = H[j];
            H[j] = temp;
        }
    }
}
