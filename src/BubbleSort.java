public class BubbleSort {
    public static void main(String[] args) {
        int[] numbers = {5, 4, 3, 6, 1};
        sort(numbers);

        for(int x : numbers)
            System.out.print(x + ", ");
    }

    static void sort(int[] numbers) {
        boolean swapped;
        for(int i = 1; i < numbers.length; i++){
            swapped = false;
            for(int j = 0; j < numbers.length - i; j++) {
                if(numbers[j] > numbers[j + 1]) {
                    swap(numbers, j, j + 1);
                    swapped = true;
                }
            }

            for(int x : numbers)
                System.out.print(x + ", ");
            System.out.println();

            if(!swapped)
                break;
        }
    }

    static void swap(int[] numbers, int i, int j) {
        int temp = numbers[i];
        numbers[i] = numbers[j];
        numbers[j] = temp;
    }
}
