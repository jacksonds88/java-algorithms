// Source: https://www.youtube.com/watch?v=FO7VXDfS8Gk&t=29s

public class LargestSquareInMatrix {
    public static void main(String[] args) {
        int[][] M = new int[][] {
                new int[]{1, 1, 0, 1, 0},
                new int[]{0, 1, 1, 1, 0},
                new int[]{1, 1, 1, 1, 0},
                new int[]{0, 1, 1, 1, 1}
        };

        System.out.println("SOLUTION: " + solution(M));

        int[][] M2 = new int[][] {
                new int[]{1, 1, 0, 1, 0},
                new int[]{1, 1, 1, 0, 0},
                new int[]{1, 1, 1, 0, 0},
                new int[]{1, 1, 1, 0, 1}
        };

        System.out.println("SOLUTION: " + solution(M2));
    }

    static int solution(int[][] M) {
        int[][] D = new int[M.length][M[0].length];

        int largest = 0;
        for(int i = 0; i < D.length; i++) {
            D[i][0] = M[i][0];
            if(D[i][0] > largest)
                largest = D[i][0];
        }

        for(int i = 0; i < D.length; i++) {
            D[0][i] = M[0][i];
            if(D[0][i] > largest)
                largest = D[0][i];
        }

        for(int i = 1; i < M.length; i++) {
            for(int j = 1; j < M[i].length; j++) {
                int top = M[i-1][j];
                int left = M[i][j-1];
                int diag = M[i-1][j-1];
                if(top == left && left == diag) {
                    D[i][j] = D[i-1][j-1] + 1;
                    if(largest < D[i][j])
                        largest = D[i][j];
                } else
                    D[i][j] = M[i][j];


            }

            System.out.println();
            for(int[] m : D) {
                for (int x : m)
                    System.out.print(x + ",");
                System.out.println();
            }
        }

        System.out.println();

        for(int[] m : D) {
            for (int x : m)
                System.out.print(x + ",");
            System.out.println();
        }

        return largest;
    }
}
