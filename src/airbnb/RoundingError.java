package airbnb;

import java.util.*;
import java.util.stream.Collectors;

public class RoundingError {
    public static void main(String[] args) {
        int T = 8;
        List<Float> prices = new ArrayList<>(3);
        prices.add(0.7f);
        prices.add(2.8f);
        prices.add(4.9f);

        List<Integer> result = solution(T, prices);
        System.out.print(String.join(",", result.stream().map(o -> o.toString()).collect(Collectors.toList())) );
        // expected output is 0,3,5
    }

    static class Node {

        int value;
        int total;
        double error;
        Node parent;
        int numParents;

        public Node(int value, int total, double error, Node parent, int numParents) {
            this.value = value;
            this.total = total;
            this.error = error;
            this.parent = parent;
            this.numParents = numParents;
        }

        public String toString() {
            return "(" + value + ", " + total + ", " + error + ", " + numParents + ")";
        }
    }

    static List<Integer> solution(int T, List<Float> prices) {
        List<Node> leaves = new LinkedList<>();
        Queue<Node> Q = new LinkedList<>();
        Q.add(new Node(0, 0,0, null, 0));

        while(!Q.isEmpty()) {
            Node n = Q.poll();
            if(n.numParents >= prices.size())
                continue;

            float price = prices.get(n.numParents);
            Node floor = new Node((int)Math.floor(price), n.total + (int)Math.floor(price), price - Math.floor(price), n, n.numParents+1);
            Node ceil = new Node((int)Math.ceil(price), n.total + (int)Math.ceil(price),Math.ceil(price) - price, n, n.numParents+1);
            Q.add(floor);
            Q.add(ceil);

            if(floor.numParents == prices.size()) {
                if(floor.total == T)
                    leaves.add(floor);
                if(ceil.total == T)
                    leaves.add(ceil);
            }
        }

        //for(Node leaf : leaves) {
        //    System.out.println(leaf);
        //}

        //System.out.println();
        LinkedList<Integer> ret = new LinkedList<>();
        double min = Double.MAX_VALUE;
        Node best = null;
        for(Node leaf : leaves) {
            Node p = leaf;
            double error = 0;
            while(p != null) {
                //System.out.print(p + "   ");
                error += p.error;
                p = p.parent;
            }

            if(error < min) {
                best = leaf;
                min = error;

            }
            //System.out.println();
        }


        while(best != null && best.numParents != 0) {
           ret.addFirst(best.value);
           best = best.parent;
        }

        return ret;
    }
}
