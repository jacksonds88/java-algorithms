package airbnb;

import java.util.Scanner;

public class Bionotes {

    /* Parse the following. Commas within quotes is allowed, and double double-quotes should be single quotes in output

        Weronika,Zaborska,njkfdsv@dsgfk.sn,"running, sci-fi",new,Krakow,25
        Ryuichi,Akiyama,jkg@ljnsfd.fjn,music,guide,Tokyo,65
        Elena,Martinez,emrt@lsofnbr.rt,"cooking, traveling",superhost,Valencia,42
        "John ""Mo""",Smith,sfn@flkaei.km,biking and hiking,,"Seattle, WA",23
     */

    static final String[] ITEMS = {"First Name", "Last Name", "Email", "Hobbies", "Misc", "Address", "Age"};

    public static void main(String[] args)
    {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT */

        Scanner scan = new Scanner(System.in);
        String s = scan.nextLine();
        System.out.println(s);

        String[] sol = solution(s);
        for(int i = 0; i < sol.length; i++)
            System.out.println(ITEMS[i] + ": " + sol[i]);
    }

    static String[] solution(String s)
    {
        int index = 0;
        String[] sol = new String[7];
        char[] C = s.toCharArray();
        for(int i = 0; i < C.length - 1; i++){
            String ret;
            if(i == 0 || C[i] == ',') {
                StringBuilder sb = new StringBuilder();
                int j = C[i] == ',' ? i + 1 : i;

                if (C[j] == '"') {
                    j = j + 1;
                    while (j < C.length) {
                        if (C[j] == '"')
                        {
                            if(C[j+1] == '"')
                            {
                                sb.append(C[j]);
                                j+=2;
                                continue;  // skip over the double quotes
                            }
                            else
                                break;
                        }
                        sb.append(C[j]);
                        j++;
                    }
                    ret = sb.toString();
                } else {
                    while(j < C.length && C[j] != ','){
                        sb.append(C[j]);
                        j++;
                    }
                    ret = sb.toString();
                }

                i = j - 1;
                sol[index++] = ret;
            }
        }

        return sol;
    }
}
